Important Notes / Recent Updates
=====================================
April 7, 2017
FOR WINDOWS 10
in powershell, type **npm install -g gulp-cli** before using gulp.
Also you need to install node sass using npm install -g node-sass from powershell.


**NOTE:** IF 'libsass' not found issue occur on gulp serve command.
Try deleting node_modules folder in repo, and run the npm install command again from powershell before running gulp serve command.


Please update this readme if there is any new install of node module or any dev dependencies including ruby gems: e.g. 26-10-2015 : add module npm install -g (module-name)



 UI
=====================================

 UI is front-end frame using SASS, SUSY, JQUERY to build re-usable components.

---

### Installation

1. Clone this repo from `git clone [url]`. Get the `[url]` from BitBucket.  
2. Run `npm install` from the root directory
3. Run `gulp serve` (may require installing Gulp globally `npm install gulp -g`)
4. Your browser will automatically be opened and directed to the browser-sync proxy address, usuall localhost:3030 or 3001
5. To prepare assets for production, run the `gulp prod` task (Note: the production task does not fire up the express server, and won't provide you with browser-sync's live reloading. Simply use `gulp serve` during development. More information below)

Now that `gulp serve` is running, the server is up as well and serving files from the `/build` directory. 

Any changes in the `/app` directory will be automatically processed by Gulp and the changes will be injected to any open browsers pointed at the proxy address.

```

Please inspect the package.json to check the dependencies of this:

- [Gulp](http://gulpjs.com/)
- [Browserify](http://browserify.org/)
- [SASS](http://sass-lang.com/)
- [JQuery](http://Jquery.com/)

Along with many Gulp libraries (these can be seen in either `package.json`, or at the top of each task in `/gulp/tasks/`).

---

### Installation Troubleshooting ###

#### Error - something to do with Compass ####

Reason and solution:
- After that, run `npm install` in the **root project directory** again, so that Ruby and Compass can be picked up.


##### Dependency injection

Dependency injection is carried out with the `ng-annotate` library. The Gulp tasks will take care of injecting any dependencies, requiring you only to specify the dependencies within the function call and nothing more.

---
```


### Responsive Grid
#### We use susy and breakpoint to write responsive behavior

We use 12 column grid to define the layouts, you may notice we extensively relay on susy container and span mixins.

#!css

```
//Sussy Grid 60em = 960px;
$susy: (
  columns: 12,
  container: 60em,
  gutters: 1 / 4,
  gutter-position: split,
  // debug: (
  //   image: show,
  //   color: rgba(#66f, .25),
  //   output: overlay,
  //   toggle: top right
  // )
);

@ include border-box-sizing();

```