var $ = require('../../jquery/vendors/jquery');

// PROJECT SCRIPT: Get My Score
$(document).ready(function() {
  hamburger();
  initChoiceSwitch();
  initRadios();
  hidePlaceAutocompleteContainerWhenScrolling();
});

/**
 * Inject the hamburger menu programmatically as GetMyScore HTML doesn't include it
 */
var hamburger = function() {
  var hamburger = $('<label for="HamburgerToggle"><span></span></label>');
  hamburger.click(function(event){

    $(this).toggleClass('menu-opened');

    if (hamburger.hasClass('menu-opened')) {
      $('td.tdLeft').show();
      $('td#tdBody').addClass('hamburger-menu-opened');
      $('#divMain').css({'overflow-x': 'hidden'});  // hide horizontal scroll bar
    } else {
      $('td.tdLeft').hide();
      $('td#tdBody').removeClass('hamburger-menu-opened');
      $('#divMain').css({'overflow-x': 'auto'}); // show horizontal scroll bar
    }
  });
  $('#tdHeading').prepend(hamburger);
};

/**v
 * Toggle class 'active' on borrowerCount choice switch
 */
var initChoiceSwitch = function() {
  $('input.borrowerCount').click(function() {
    if (!$(this).hasClass('active')) {
      $('input.borrowerCount').removeClass('active');
      $(this).addClass('active');
    }
  });
};

/**
 * Remove the "check" attribute from radios that are checked by default
 * initiate a virtual click on the label to re-trigger the radio's selection
 */
var initRadios = function() {
  var checked_radios = $('.pepper.radiodiv input[type=radio][checked]'),
    checked_radios_label = $('.pepper.radiodiv input[type=radio][checked] ~ label');

  checked_radios.removeAttr('checked');
  checked_radios_label.click();
};

/**
 * The Google Place Autocomplete container doesn't stay in one position when scrolling.
 * Solution: Hide it straight away when scrolling.
 */
var hidePlaceAutocompleteContainerWhenScrolling = function() {
  $('#divMain').scroll(function() {
    $('.pac-container').hide();
  });
};