var $ = require('../../jquery/vendors/jquery');

$(document).ready(function() {
    var RadioControls = '';
    var totalRadioControls = $('#CompoSettings .radio-control').length;
    var initials = $('#CompoSettings input[type=radio]:checked');
    $('#CompoSettings .radio-control').each(function(i) {
        if ($(this).find('input[type=radio]:first').attr('name') != undefined) {
            RadioControls += 'input[name=' + $(this).find('input[type=radio]:first').attr('name') + ']';
            if (i != totalRadioControls - 1) {
                RadioControls += ',';
            }
        }
    });

    $(RadioControls).on('change', function(e) {
        e.preventDefault();
        var CSSClass = $(this).attr('value');
        var ControlName = $(this).attr('name');
        var flexiBlock = ($('.flexi-block').hasClass('selected')) ? $('.flexi-block.selected') : $('.flexi-block');
        if (!flexiBlock.hasClass(CSSClass)) {
            //clear all justify content class first
            $("input[name=" + ControlName + "]").each(function() {
                flexiBlock.removeClass($(this).attr('value'));
            });
            //then apply selected class;
            flexiBlock.addClass(CSSClass);
        }
        switch (ControlName) {
            case 'bg-settings':
                if (flexiBlock.hasClass('bg-image')) {
                    flexiBlock.css({
                        "background-image": "url('" + flexiBlock.attr('data-url') + "')"
                    });
                    
                    $('#BgUrl').closest('fieldset').show();
                } else {
                    flexiBlock.removeAttr('style');
                    $('#BgUrl').closest('fieldset').hide();
                }
                if(CSSClass == 'bg-light' || CSSClass == 'bg-dark') {
                      $('.flexi-layout').addClass(CSSClass);
                } else {
                  $('.flexi-layout').removeClass('bg-light bg-dark');
                }
                break;
            case 'image-settings':
                var imageSrc = '';
                switch(CSSClass) {
                    case 'image-sticky-bottom':
                    imageSrc = 'https://www.pepper.com.au/.imaging/stk/pepper-blade/tile225x468/dam/pepper/lending/help-centre/getting-started/5-important-qustions-to-ask-about-a-personal-loans-780x375/jcr:content/5-important-qustions-to-ask-about-a-personal-loans-780x375.jpg.jpg';
                    break;
                    case 'half-image':
                    imageSrc = 'https://www.pepper.com.au/.imaging/stk/pepper-blade/pep-original/dam/pepper/lending/home-loans/portholes-banners/half-blade-refinancer/jcr:content/half-blade-refinancer.jpg.jpg';
                    break;
                    case 'image-sticky-top':
                    if($('.flexi-block').hasClass('card-style')) {
                        //square image for card style like HC Grid
                        imageSrc = 'https://www.pepper.com.au/.imaging/stk/pepper-blade/tile225x468/dam/pepper/lending/help-centre/getting-started/5-important-qustions-to-ask-about-a-personal-loans-780x375/jcr:content/5-important-qustions-to-ask-about-a-personal-loans-780x375.jpg.jpg'
                    } else {
                        //rounded image for porthole.
                        imageSrc = 'https://www.pepper.com.au/.imaging/stk/pepper-blade/pep-original/dam/pepper/lending/home-loans/portholes-banners/porthole-refinance-v2/jcr:content/porthole-refinance-v2.png.png.png'
                    }
                    break;
                    case 'image-right':
                    case 'image-left':
                    imageSrc = 'https://www.pepper.com.au/.imaging/stk/pepper-blade/pep-original/dam/pepper/lending/home-loans/portholes-banners/porthole-refinance-v2/jcr:content/porthole-refinance-v2.png.png.png'
                    break;
                } //switch of image class
                flexiBlock.find('.image-block img').attr('src', imageSrc);
                break;
        }
    });

    $('#CompoBehaviour').on('change', function(e){
      //reset to Default;
      initials.prop('checked', true).trigger('change');
      switch($(this).val()) {
        case 'hero':
        $('.flexi-block, .flexi-layout').removeClass('item').addClass($(this).val());
        $('.flexi-block .image-block img').attr('src', 'https://www.pepper.com.au/.imaging/stk/pepper-blade/pep-original/dam/pepper/lending/home-loans/portholes-banners/half-blade-refinancer/jcr:content/half-blade-refinancer.jpg.jpg');
        $('.block-item-select, .add-remove-items, #ContentStyle').hide(); 
        $('#AreaCoverage, #VerticalContentAlign').show();
        $('input[value=image-sticky-top], input[value=image-sticky-bottom]').parent().hide();
        break;
        case 'item':
        $('.flexi-block, .flexi-layout').removeClass('hero').addClass($(this).val());  
        $('.flexi-block .image-block img').attr('src', 'https://www.pepper.com.au/.imaging/stk/pepper-blade/pep-original/dam/pepper/lending/home-loans/portholes-banners/porthole-refinance-v2/jcr:content/porthole-refinance-v2.png.png.png');
        $('.block-item-select, .add-remove-items, #ContentStyle').show();
        $('#AreaCoverage, #VerticalContentAlign').hide();
        $('input[value=image-sticky-top], input[value=image-sticky-bottom]').parent().show();
        break;
      }
    });
    
    $('#BlockBehaviour').on('change', function(e){
        $('.add-remove-items').show();
        $('.flexi-layout').removeClass('one-column two-columns three-columns four-columns').addClass($(this).val());
    });

    $('#AddItem').on('click', function(e){
      e.preventDefault();
      $('.flexi-layout>.container').append($('.flexi-block:first').clone());
      $('#RemoveItem').show();
    });

    $('#RemoveItem').on('click', function(e){
      e.preventDefault();
      $('.flexi-block:last').remove();
      if($('.flexi-block').length == 1) {
        $(this).hide();
      }
    });

    

    $('#BgUrl').on('blur', function(e) {
        e.preventDefault();
        if ($(this).val() != '' && $(this).val().indexOf('http') >= 0) {
            $('.flexi-block').css({
                "background-image": "url('" + $(this).val() + "')"
            });
        }
    });

    //Selected Item block
    $('body').on('click', '.flexi-block.item', function(e){
        e.preventDefault();
        if(!$(this).hasClass('selected')) {
            $('.flexi-block.selected').removeClass('selected');
            $(this).addClass('selected');
        } else {
            $(this).removeClass('selected');
        }
    });
});