var $ = require('./vendors/jquery'),
    powerange = require('./vendors/powerange'),
	  //add your js module here;
    hamburgerMenu = require('./components/hamburger-menu'),
    loginMenu = require('./components/login-menu'),
    contentSearch = require('./components/content-search'),
    pepperTestimonial = require('./components/pepper-testimonial'),
    carousels = require('./components/carousels'),
    preventDefault = require('./components/prevent-default'),
    sideNav = require('./components/side-nav-sticky'),
    videoHero = require('./components/hero-video'),
    pageTabs = require('./components/page-tabs'),
    rangeSlider = require('./components/range-slider'),
    heroImage = require('./components/hero-image'),
    heightFix = require('./utility/height-fix'),
    copyToClipBoard = require('./utility/email-this'),
    emailThis = require('./utility/copy-to-clipboard'),
    searchFilter = require('./utility/search-filter'),
    shortForm = require('./components/short-form'),
    sanitizeComma = require('./utility/sanitize-comma'),
    numberIncrement = require('./utility/number-increment'),
    expandAble = require('./utility/expandable'),
    sortGrid = require('./utility/sort-grid'),
    rateTabs = require('./components/rate-tabs'),
    videoList = require('./components/video-list'),
    secondaryNav = require('./components/secondary-nav'),
    Analytics = require('./components/analytics'),
    lightBox = require('./components/light-box'),
    datePicker = require('./utility/date-picker'),
    breadCrumb = require('./components/bread-crumb'),
    fileUpload = require('./components/file-upload');
    noticeCookie = require('./components/notice');

// No modular approach here, run script straight away.
require('./components/search-results');

// PROJECT SCRIPT
$(document).ready(function() {
  hamburgerMenu();
  loginMenu();
  contentSearch();
  pepperTestimonial();
  carousels();
  preventDefault();
  sideNav();
  videoHero();
  rangeSlider();
  heroImage();
  pageTabs();
  heightFix();
  copyToClipBoard();
  searchFilter();
  sanitizeComma();
  numberIncrement();
  expandAble();
  sortGrid();
  shortForm();
  rateTabs();
  videoList();
  secondaryNav();
  Analytics();
  lightBox();
  datePicker();
  breadCrumb();
  emailThis();
  fileUpload();
  noticeCookie();
  //cookies(); //use for test of uk only.
});
/*! End $(document).ready() */