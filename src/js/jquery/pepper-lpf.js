var $ = require('./vendors/jquery'),
    powerange = require('./vendors/powerange'),
	  //add your js module here;
    preventDefault = require('./components/prevent-default'),
    heightFix = require('./utility/height-fix'),
    shortForm = require('./components/short-form'),
    sanitizeComma = require('./utility/sanitize-comma'),
    numberIncrement = require('./utility/number-increment')
    lpfCalculator = require('./components/lpf-calculator');

// PROJECT SCRIPT
$(document).ready(function() {
  preventDefault();
  heightFix();
  shortForm();
  sanitizeComma(); 
  numberIncrement(); 
  lpfCalculator();
});
/*! End $(document).ready() */