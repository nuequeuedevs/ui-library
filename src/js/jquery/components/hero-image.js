var $ = require('../vendors/jquery'),
  scroll = require('../utility/scroll'),
  heroImage = $('.hero-image');

exports.init = function() {
  $('body').on('click', 'section.hero-image a.scroll-button, .hero-video a.scroll-button', function(event) {
    var sectionId = $(this).attr('href');
    event.preventDefault();
    scroll.scrollToSection(sectionId);
  });
};

exports.makeHeroFullPage = function() {
  var getPageHeight = $(window).height(),
      appNav = $("nav.app-nav"),
      appNavHeight = appNav.height();

  heroImage.height(getPageHeight - appNavHeight); //300 Jquery Bug + header.
};

exports.replaceBackgroundImage = function() {
  // heroImage.css({
  //   "background-image": "url('" + heroImage.data('image-url') + "')"
  // });
};

module.exports = function() {
  exports.init();

  if (heroImage.hasClass('full-screen')) {
    $(window).resize(exports.makeHeroFullPage);
    exports.makeHeroFullPage();
  }

  exports.replaceBackgroundImage();
};

