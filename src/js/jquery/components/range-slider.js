var $ = require('../vendors/jquery'),
    formValidation = require('../utility/form-validation'),
    Powerange = require('../vendors/powerange');

module.exports = function() {
  var initChangeInput;
  if ($('.js-check-change').length > 0) {
    var changeInput = document.querySelector('.js-check-change');
    var minVal = $('#js-display-change').data('min');
    var maxVal = $('#js-display-change').data('max');
    var steps = $('#js-display-change').data('steps');
    var leftOf = $('#pepperShortForm').length > 0 ? 61 : 59;
    if ($(window).width() >= 960 && $('.aside-form').length <= 0) {
      $('.display-box').css('left', -leftOf + 'px');
    } else {
      if($('.aside-form').length < 0) {
       $('.display-box').css('left', 0);  
      }
    }
    //console.log(minVal + ' is not before');
    if (minVal === null || minVal === undefined || minVal === '') {
      minVal = '0';
    }
    if (maxVal === null || maxVal === undefined || maxVal === '') {
      maxVal = '1000000';
    }
    if (steps === null || steps === undefined || steps === '') {
      steps = '1000';
    }
    //console.log(minVal + '\n' + maxVal + '\n'+ steps);
    setTimeout(function () {
      $('.display-box').fadeIn('fast');
      var initChangeInput = new Powerange(changeInput, {
        min: parseInt(minVal),
        max: parseInt(maxVal),
        step: parseInt(steps),
        decimal: false
      });
      changeInput.onchange = function () {
        document.getElementById('js-display-change').value = changeInput.value;
        if ($(window).width() >= 960 && $('.aside-form').length <= 0) {
          var handleLeft = parseInt($('.range-handle').css('left'));
          $('.display-box').css('left', (handleLeft - leftOf) + 'px');
        } else {
          if($('.aside-form').length < 0) {
           $('.display-box').css('left', 0);  
          }
        }
      };

      $('body').on('blur', '#js-display-change', function (e) {
        e.preventDefault();
        var roundVal = Math.round($(this).val());
        if (roundVal < minVal) {
          roundVal = minVal;
        }
        if (roundVal > maxVal) {
          roundVal = maxVal;
        }
        $(this).val(roundVal);
        initChangeInput.setStart(roundVal);
        $(this).val(roundVal);
        var handleLeft = parseInt($('.range-handle').css('left'));
        if ($(window).width() >= 960 && $('.aside-form').length < 0) {
          $('.display-box').css('left', (handleLeft - leftOf) + 'px');
        }
        $('.slide-error').hide();
      });

      $('body').on('keydown', '#js-display-change', function (e) {
        if (e.keyCode == 13) {
          $('#js-display-change').trigger('blur');
        }
        formValidation.preventTypeInNonNum(e);
      });


    }, 1500);


    $(window).resize(function () {
      initChangeInput.setStart(0);
    });

  } // if power range exists on page.
}