var $ = require('../vendors/jquery');

module.exports = function() {
    $(document).ready(function() {

        $('body select, input[type=hidden]').each(function() {
            $(this).attr('name', 'lpfCalcM.' + $(this).attr('name'));
        });

        $('body').on('change', '#loan-purpose, #product-type, #product, #payment-method', function(e) {
            e.preventDefault();
            PostForm();
        });

        $('body').on('blur', '#loan-amount, #property-value', function(e) {
            e.preventDefault();
            PostForm();
        });

        $(window).on('resize',function(){
            resizeiFrame();
        });


    });

    $(window).on('load',function(){
        resizeiFrame();
    });

    function resizeiFrame() {
        var height = document.getElementsByTagName("html")[0].scrollHeight;
        window.parent.postMessage(["setHeight", height], "*"); 
    }

    function PostForm() {
        var isValidInputs = true;
        $('.currency-mark input').each(function() {
            $(this).santizeCommas();
            if (!ValidateInput($(this))) {
                isValidInputs = false;
            }
        });
        if (isValidInputs) {
            var lpf_calc = $('#lpf-calc-panel').serialize(); //GetLPFCalculation();
            $.ajax({
                method: 'POST',
                url: '/LPF/SubmitWholeForm',
                data: lpf_calc,
            }).done(function(data) {
                $('#right_col').html(data);
                $('.currency-mark input').each(function() {
                    $(this).manageCommas();
                });
            }).fail(function(jqXHR, textStatus, error) {
                alert(error);
            });
        }

    }

    function ValidateInput(el) {
        var pattern = el.attr("pattern");
        var isVisible = el.closest('fieldset').is(':visible');
        var isNotChoice = el.closest('fieldset').hasClass('choice-switch');
        if (el.attr("required") && isVisible && !isNotChoice) {
            var val = el.val();
            if (
                (el.is("[type=checkbox]") && !el.is(":checked")) || // Required Checkboxes
                (el.closest("fieldset.date").length == 1 && !formValidation.isValidDate(val)) || // check for valid date
                (el.closest("fieldset.date-picker").length == 1 && !formValidation.isValidDate(val)) ||
                (el.closest("fieldset.validate-age").length == 1 && !formValidation.isValidAge(val)) ||
                (val === "" || val == el.attr("placeholder")) || // Required inputs
                (el.is('select') && el.prop('selectedIndex') == 0) ||
                (el.is("[type=email]") && !formValidation.isValidEmail(val)) ||
                (pattern && !val.match(new RegExp(pattern))) || // If we have a pattern attributes
                el.is("[data-confirm]") && val != $("#" + el.attr("data-confirm")).val()
            ) { // Custom confirmation boxes
                ShowValidationError(el);
                return false;
            } else {
                HideValidationError(el);
                return true;
            }
        } else {
            return true;
        }
    }

    function ShowValidationError(el) {
        el.css('border-color', '#e00024');
        if (el.hasAttr('type') && el.attr('type') == 'checkbox') {
            el.parent().find('.error').slideDown('fast', function() {});
        } else {
            el.parent().find('.error').slideDown('fast', function() {});
        }
        return false;
    }

    function HideValidationError(el) {
        el.css('border-color', '#66CC99');
        if (el.hasAttr('type') && el.attr('type') == 'checkbox') {
            el.parent().find('.error').slideUp('fast', function() {});
        } else {
            el.parent().find('.error').slideUp('fast', function() {});
        }
    }
}