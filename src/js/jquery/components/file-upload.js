var $ = require('../vendors/jquery');

module.exports = function() {
    var loadedFiles = 0;
    var totalsize = 0;
    var sizeLimit = $('#LoadFile').attr('data-maxSize');
    var acceptExtensions = $('#LoadFile').attr('data-accept');

    $('body').on('click', '#LoadFile', function(e) {
        e.preventDefault();
        loadedFiles++;
        var inputFile = "<input name='files" + loadedFiles + "' accept='"+ acceptExtensions +"' id='Files" + loadedFiles + "' type='file'/>";
        //checkout/guess user previously click on browser but then choose cancel on OS dialog
        if ($(this).next().is('input[type=file]') && $(this).next().val().length == 0) {
            loadedFiles--;
        } else {
            $(inputFile).insertAfter($(this));
        }
        $('#Files' + loadedFiles).trigger('click');
    });
    //add upload file event
    $('body').on('change', '.file-upload input[type=file]', function(e) {
        if (!$(this).val().length == 0) {
            var files = e.target.files;
            var count = files.length;
            var size = files[0].size / 1000; //into KB if > 1000 MB
            var approxSize = Math.round((size / 1024).toFixed(3) * 100) / 100;
            var isSameFile = false;
            var sizeLimit = $('#LoadFile').attr('data-maxSize');
            var acceptedExtensions = $(this).attr('accept');
            var hasExtension = (files[0].name.indexOf('.') > 0) ? true : false;
            var isValidExtension = (acceptedExtensions.indexOf(files[0].name.toString().toLowerCase().split('.').pop()) >= 1) ? true : false;
            if(!hasExtension) {
                isValidExtension = true;
            }
            if (approxSize <= sizeLimit) {
                //Same file check
                $('input[type=file]').each(function() {
                    if (files[0].name == $(this).attr('data-value')) {
                        isSameFile = true;
                    }
                });
                if (isSameFile) {
                    $(this).remove();
                    loadedFiles--;
                } else {
                    if(isValidExtension) {
                    totalsize = totalsize + files[0].size;
                    //Size limit check
                    if ((totalsize / 1024) / 1000 > sizeLimit) {
                      var newSize = ((totalsize / 1024) / 1000).toFixed(1);
                      $(this).remove();
                      loadedFiles--;
                      alert('Size limit exceeding to '+ newSize + 'MB, cannot upload this file.');
                      totalsize = totalsize - files[0].size;
                      //$('#LoadFile').attr('disabled', 'disabled');
                    } else {
                        $('.size-limit').text('Limit ' + sizeLimit + 'MB/ Used: ' + ConvertSize(totalsize));
                        //set the value to compare matching file;
                        $(this).attr('data-value', files[0].name);
                        //append the file into list
                        $('.file-list').append("<li><span>" + files[0].name + "</span><i class='fa fa-times-circle' title='Remove' data-link='" + $(this).attr('id') + "'' data-size='"+ files[0].size +"'></i></li>").show();
                     }
                    } else {
                        alert('Invalid File type, cannot upload.');
                        $(this).remove();
                        loadedFiles--;
                    }
                }
            } else {
                alert('Max 5MB file allowed');
                $(this).remove();
                loadedFiles--;
            }
        }
    }); //on change of file upload

    //Delete File Functionality on delete icon
    $('body').on('click', '.file-upload .fa-times-circle', function(e){
        e.preventDefault();
        var deleteFile = $(this).attr('data-link');
        var fileSize = parseInt($(this).attr('data-size'));
        totalsize = totalsize - fileSize;
        $('.size-limit').text('Limit ' + sizeLimit + 'MB/ Used: ' + ConvertSize(totalsize));
        $(this).closest('li').remove();
        if($('.file-list').find('li').length == 0) {
            $('.file-list').hide();
        }
        $('#'+deleteFile).remove();
        loadedFiles--;
        if((totalsize / 1024) / 1000 < sizeLimit) {
             $('#LoadFile').removeAttr('disabled');
        }
        //ask
    });

    function ConvertSize(size) {
        var sOutput = size + " bytes";
        // add proper size unit
        for (var aMultiples = ["KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], nMultiple = 0, nApprox = totalsize / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
        sOutput = Math.ceil(nApprox.toFixed(3) * 100) / 100 + aMultiples[nMultiple];
        }        
       return sOutput;
    }
}