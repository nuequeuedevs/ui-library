var $ = require('../vendors/jquery');

function setSecondaryNav() {
  var nav = $('.secondary-nav'),
    menu = $('.secondary-nav > menu'),
    active_item = $('.secondary-nav a.active');

  if ($(window).width() <= 1024) {
    active_item.click(function(event) {
      nav.toggleClass('opened');
      event.preventDefault();
    });
    menu.prepend(active_item);
  }
}

module.exports = function() {
  setSecondaryNav();

  // When resize finishes (assumed 250ms), then set the secondary nav.
  var resizeTimer;
  $(window).resize(function() {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function(){
      setSecondaryNav();
    }, 250)
  });
};