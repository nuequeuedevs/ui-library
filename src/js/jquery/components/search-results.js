var $ = require('../vendors/jquery'),
    checkDevices = require('../utility/check-device');

module.exports = function() {};
// var searchApiVersion = '1'; 
// google.load('search', searchApiVersion, {language: 'en'});
// google.setOnLoadCallback(function() {
//   var customSearchOptions = {};
//   var customSearchControl =   new google.search.CustomSearchControl('013781352769439467301:d2eybxbev1m', customSearchOptions);
//   customSearchControl.setResultSetSize(10);
//   var options = new google.search.DrawOptions();
//   options.setAutoComplete(true);
//   customSearchControl.setSearchCompleteCallback(null,function() { searchCompleteCallback(customSearchControl) });
//   customSearchControl.draw('customGoogleSearchDIV', options);
//   // Run a query
//   customSearchControl.execute(checkDevices.getParameterByName('queryStr'));
// }, true);

// function searchCompleteCallback(customSearchControl) {
//   //console.log(customSearchControl)
//   var r = /\d/;
//   var s = customSearchControl.Ig.innerText;
//   var count = s.match(r)? s.match(r)[0] : 0;
//   var actualSearchedText = $(".gs-spelling-original a b i").length > 0 ? $(".gs-spelling a b i").html() : customSearchControl.gf;
//   try {
//     digitalData.events.push({
//       "type": "analytics",
//       "event": "internalSearch",
//       "searchTerm": actualSearchedText,
//       "searchResultCount": count
//     });
//   } catch (e) {}
// }



//ABOVE GSCE is obselleted by Google.
//API v2 used below based on MB-3115
  (function() {
        var cx = '013781352769439467301:d2eybxbev1m';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
})();

$(window).on('load', function(){
	if($('#resInfo-0').length) {
		var count = $('#resInfo-0').text().replace( /^\D+/g, '').split(' ')[0];
		var keyword = checkDevices.getParameterByName('queryStr').replace('+', ' ');
		try {
	    digitalData.events.push({
	      "type": "analytics",
	      "event": "internalSearch",
	      "searchTerm": keyword,
	      "searchResultCount": count
	    });
	  } catch (e) {
	  }
	} //if result loaded
});