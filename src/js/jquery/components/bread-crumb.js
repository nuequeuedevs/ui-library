var $ = jQuery = require('../vendors/jquery'),
	checkDevice = require('../utility/check-device');

module.exports = function() {
    $('document').ready(function() {
    	var hasBreadCrumb = ($('body').find('.bread-crumb').length == 1) ? true : false;
        //console.log(hasBreadCrumb);
    	if(hasBreadCrumb) {
    		var cssClass;
    		switch($('.bread-crumb menu').find('a').length) {
    			case 1:
    			cssClass = 'hidden';
    			break;
    			case 2:
    			default:
    			cssClass = 'two-level';
    			break;
    			case 3:
    			cssClass = 'three-level';
    			break;
    			case 4:
    			cssClass = 'four-level';
    			break;
    		}
    		$('.bread-crumb').addClass(cssClass).show();
    	}
        if (hasBreadCrumb && ($('body').find('.hero-carousel').length == 1 || $('body').find('.hero-image').length == 1 || $('.bread-crumb').next().hasClass('content-search'))) {
            $('.bread-crumb').addClass('has-hero').show();
        }
        if(hasBreadCrumb && $('body').find('.secondary-nav').length == 1 && !$('.bread-crumb').next().hasClass('content-search')) {
        	$('.bread-crumb').addClass('has-sec-nav');
        	$('.secondary-nav').addClass('has-breadcrumb').show();
        }

			// MB-2038: on mobile, Javascript moves a.home outside of menu and prepend it as a direct child of nav.bread-crumb
			
				MobileCrumb();
			

        //MB-2274
        $('body').on('click', '.bread-crumb menu a:last-child',function(e) {
            e.preventDefault();    
        });

        $(window).resize(function(){
            MobileCrumb();
        });

    });

    function MobileCrumb() {
        
        if($(window).width() <= 640 && !$('.bread-crumb menu').hasClass('mobile-adjusted')) {
        var nav = $('.bread-crumb'),
            home_link = nav.find('menu a:first-child'),
            menu = nav.find('menu'),
            scrollTo = 0;

                // for Broker pages, add the '.home' class and prepend the home icon
                if (!home_link.hasClass('home')) {
                    home_link.addClass('home').prepend('<i class="fa fa-home"></i>');
                }

                home_link.prependTo(nav);
                menu.addClass('mobile-adjusted');

                // calculate the scrollTo position by summing the width of all the <a>
                menu.find('a').each(function(){
                        scrollTo += $(this).width();
                });
                menu.scrollLeft(scrollTo);
        } else if($(window).width() > 640 && $('.bread-crumb menu').hasClass('mobile-adjusted')) {
                $('.bread-crumb > a').prependTo($('menu.mobile-adjusted'));
                $('menu.mobile-adjusted').removeClass('mobile-adjusted');    
        }
    }
}