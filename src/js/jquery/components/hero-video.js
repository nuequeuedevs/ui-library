var $ = require('../vendors/jquery');

module.exports = function() {

    HeroVideo();

function HeroVideo() {
    var getPageHeight = $(window).height(),
        videoWrapperHeight = getPageHeight - 60,
        videoWrapperWidth = $(window).width(),
        videoWrapper = $('.hero-video .video-wrapper'),
        video = videoWrapper.find('video').get(0);

    if (typeof videoWrapper !== 'undefined' && typeof video !== 'undefined') {
        videoWrapper.height(videoWrapperHeight);
        video.addEventListener("canplay", function() {
            video.play();
        });

        if (videoWrapperWidth / videoWrapperHeight <= 16 / 9) {
            $(video).css({
                "height": "150%",
                "width": "auto"
            })
        } else {
            $(video).css({
                "height": "auto",
                "width": "150%"
            })
        }

        videoWrapper.css({
            'background-image': 'url(' + videoWrapper.attr('data-image-url') + ')'
        })
    }
}

}