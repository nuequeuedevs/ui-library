var $ = require('../vendors/jquery');

//all preventDefault behaviours like details summary collapse in chrome will prevent here
module.exports = function() {
    $('body').on('click',
    	'summary, details, a.completed, .bio-tile a.disabled',
    	function(e) {
        e.preventDefault();
    	});

}