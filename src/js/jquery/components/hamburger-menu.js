var $ = require('../vendors/jquery');

module.exports = function() {
    $('body').on('click', '.level-one li>a.has-sub, .level-two li>a.has-sub', function(e) {
          e.preventDefault();
          var _submenu = $(this).next('ul').attr('class').split(' ')[0];
          if($(window).width() >= 960) {
          var isActive = $(this).hasClass('active');
          $('.nav-login.active').trigger('click');
          if (!isActive) {
              ToggleMenu();
              $(this).addClass('active').next('.' + _submenu).addClass('active').slideDown('fast').show();
          }  
        }
        else {
          $(this).toggleClass('active').next('.' + _submenu).toggleClass('active');
        } 
        
    });

    $('body').on('click', '.level-two>li>a:not(.has-sub)',function(){
      ToggleMenu();
    });

    //window resize doesn't work with Jquery 'on' delegation.
    $(window).resize(function() {
        $('#HamburgerToggle').prop('checked', false);
    });

    $('body').on('click', function(e) {
        var isMenuOverlayClicked = (e.target.nodeName == 'MENU') ? true : false;
        var isMobile = ($(window).width() < 960) ? true : false;
        if (isMobile && $('#HamburgerToggle').is(':checked') && isMenuOverlayClicked) {
            e.preventDefault();
            $('#HamburgerToggle').prop('checked', false);
          } else if(!isMobile && $('.level-three').hasClass('active')) {
            var bodyArea = $(e.target).closest('main').hasClass('app-content');
            var desktopNav = $(e.target).closest('nav').hasClass('desktop-nav');
            var appHeader = $(e.target).hasClass('app-header');
            var MegaNavNoButton = $(e.target).hasClass('nav-main');
            if(bodyArea || appHeader || MegaNavNoButton || desktopNav) {
              $('.level-three.active').slideUp('fast',function(){
                $('.level-three.active').removeClass('active').prev().removeClass('active');
              });
            }
          }
    });

    function ToggleMenu() {
        // if ($(window).width() < 640) {
        //     el.addClass('active').next('.' + submenu).addClass('active');
        // } else {
        //     el.addClass('active').next('.' + submenu).slideDown('fast').addClass('active');
        // }
        $('.level-three.active').slideUp('fast');
        $('.level-three.active, .level-two li>a.has-sub').removeClass('active');
    }

    //reset sub menu on 2nd level no sub click;
    $('body').on('click', ".level-two li>a:not('.has-sub')", function(e) {
        $(this).blur();
        $('.level-two a.has-sub.active').removeClass('active');
        $('.level-three.active').removeClass('active');
    });

    //MB-2304
    $('body').on('click', '.has-sub-links>a:first-child', function(e){
      e.preventDefault();
      if($(this).parent().hasClass('active')) {
        $(this).parent().removeClass('active');
        $(this).find('i').removeClass('fa-minus').addClass('fa-plus');
      } else {
        $('.has-sub-links').removeClass('active');
        $('.has-sub-links>a').find('i').removeClass('fa-minus').addClass('fa-plus');
        $(this).parent().addClass('active');
        $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
      }
    });
}