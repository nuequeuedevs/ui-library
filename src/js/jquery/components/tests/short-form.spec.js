var shortForm = require('../short-form.js'),
    should = require('should');

describe("Short form module", function() {
  it("should be a defined object", function() {
    shortForm.should.be.an.instanceOf(Object);
  });
});