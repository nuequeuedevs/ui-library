var $ = jQuery = require('../vendors/jquery');

module.exports = function() {

    $('body').on('click', '.video-list>ul a', function(e) {
        e.preventDefault();
        var thisHref = $(this).attr('href');
        var isLastLink = $(this).parent();
        var vdoIntro = $(this).attr('data-vdointro');
        vdoIntro = '<h3>Introduction</h3>' + vdoIntro;
        if (!isLastLink.is(':last-child')) {
            $('.call-next-video').show().attr('href', isLastLink.next().find('a').attr('href')).attr('data-vdointro', isLastLink.next().find('a').attr('data-vdointro'));
        } else {
            $('.call-next-video').hide();
        }
        if (thisHref != '' && thisHref != '#') {
            $('.video-list li a').removeClass('active');
            $(this).addClass('active');
            $('.video-gallery .container footer').html(vdoIntro);
            $('.video-gallery iframe').attr('src', thisHref);
            ScrollToVideo();
        } else {
            console.log('No Link found');
        }
    });

    $('body').on('click', '.call-next-video', function(e) {
        e.preventDefault();
        var nextVdo = $(this).attr('href');
        var vdoIntro = '';
        vdoIntro = '<h3>Introduction</h3>' + $(this).attr('data-vdointro');
        if (nextVdo != '' && nextVdo != '#') {
            var nextToNextVideo;
            var nextToNextVideoIntro;
            var isLastLink;

            $('.video-list li a').removeClass('active');

            $('.video-list li').each(function() {
                var thisAnchor = $(this).find('a');
                var listItemSrc = thisAnchor.attr('href');
                if (nextVdo == listItemSrc && listItemSrc != '' && listItemSrc != '#') {
                    thisAnchor.addClass('active');
                    isLastLink = $(this).is(':last-child');
                    nextToNextVideo = $(this).next().find('a').attr('href');
                    nextToNextVideoIntro = $(this).next().find('a').data('vdointro');
                }
            });
            $('.video-gallery .container footer').empty().html(vdoIntro);
            $('.video-gallery iframe').attr('src', nextVdo);
            ScrollToVideo();
            if (isLastLink) {
                $(this).hide();
            } else {
                $(this).attr('href', nextToNextVideo);
                $(this).attr('data-vdointro', nextToNextVideoIntro);
            }
        } //not hash or empty;
    });

    function ScrollToVideo(extraMargin) {
        if ($(window).width() < 768) {
            console.log($(".video-gallery").offset().top + '\n' + extraMargin);
            if(extraMargin == 'undefined'){extraMargin = -60}
            $('html, body').animate({
                scrollTop: $(".video-gallery").offset().top + extraMargin
            }, 500);
        }
    }

}