var $ = require('../vendors/jquery');

module.exports = function() {
$('body').on('click','.nav-login',function(e) {
	$('.level-three.active').slideUp();
    $('.level-three.active, .level-two li>a.has-sub').toggleClass('active');
	$(this).toggleClass('active');
	$('.nav-login-list').slideToggle('fast');
});
}