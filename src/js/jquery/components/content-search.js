var $ = require('../vendors/jquery');

/*
 Put white background on .help-search input field and reverse
 to solve the anti-aliasing issue with border-radius on the fieldset.help-search
 */
module.exports = function() {
  $('body').on('focus', 'fieldset.help-search input[type=search]', function() {
    $(this).parent().css('background-color', '#fff');
  });

  $('body').on('blur', 'fieldset.help-search input[type=search]', function() {
    $(this).parent().css('background-color', 'rgba(0,0,0,0.5)');
  });
};
