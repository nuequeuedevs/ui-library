var $ = jQuery = require('../vendors/jquery');

module.exports = function() {

  //generic lightbox controls
  $('body').on('click', '.call-generic-lightbox', function (e) {
    e.preventDefault();

    var targetModal = "";

    targetModal = $(this).attr("href");
    var modalType = targetModal.startsWith('#')?"modal":( targetModal.startsWith('http:')?"iframe":"relative" );

    if(targetModal){
      var lightboxContent = $(".light-box.generic").find(".content");
      var lightboxTitle = $(".light-box.generic").find(".title");
      switch(modalType) {
          case "modal": lightboxContent.html($(targetModal).find("article").html());
                        lightboxTitle.html($(targetModal).find("label").html());
                        break;
          case "iframe":  lightboxContent.html("<iframe src='"+targetModal+"' width='100%' height='100%'/>");
                          lightboxTitle.html($(this).attr("data-lb-title") || "");
                          break;
          case "relative":  lightboxContent.load(targetModal);
                            lightboxTitle.html($(this).attr("data-lb-title") || "");
                            break;

          default: break;
      }
    }

    $(".generic").css("display", "block");
  });

  $('body').on('click', '.light-box.generic .close-bt', function (e) {
    e.preventDefault();
    $(this).closest('.light-box.generic').css("display", "none");
  });

  //expand - compress
  $('body').on('click', '.light-box.generic .screen-bt', function (e) { 
    var popupBody = $(".light-box.generic section.popupbody");
    var articleContent = $(".light-box.generic section.popupbody article.content");
    if ($(this).hasClass('expand')) {
      popupBody.css("max-width", "100%");
      popupBody.css("height", "100%");
      articleContent.css("max-height", "95%");
      articleContent.css("height", "95%");
      $(this).removeClass('expand').addClass('compress').html('<i class="fa fa-compress"></i>');
    } else {
      if ($(window).width() >= 1024) {
        popupBody.css("max-width", "60em");
      }else{
        popupBody.css("max-width", "90%");
      }
      popupBody.css("height", "auto");
      articleContent.css("max-height", "300px");
      $(this).removeClass('compress').addClass('expand').html('<i class="fa fa-expand"></i>');
    }

  });

};