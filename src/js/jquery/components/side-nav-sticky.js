var $ = require('../vendors/jquery');

module.exports = function() {
	//side-nav
    $('body').on('mouseover', '.side-nav a', function(e) {
        e.preventDefault();
        $(this).closest('menu').css('right', '0');
    });

    $('body').on('mouseout', '.side-nav a', function(e) {
        e.preventDefault();
        $(this).closest('menu').css('right', '-227px');
    });

    $(window).on('scroll', function() {
        var scrollTop = $(window).scrollTop();
        var matchThis = 0;
        if (scrollTop == 0) {
            $('menu.side-nav').css('right', '0');
        } else {
            $('menu.side-nav').css('right', '-227px');
        }
    });
}