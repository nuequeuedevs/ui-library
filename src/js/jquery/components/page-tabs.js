var $ = require('../vendors/jquery'),
checkDevices = require('../utility/check-device'),
scrollPage = require('../utility/scroll.js');

module.exports = function() {

    //Page tabs
    $(document).ready(function() {
        if ($('body ul.page-tabs').length > 0) {
        //$('ul.page-tabs').css('display', 'none');
        var whichContacts = checkDevices.getParameterByName('contact');
        var tabIndex = 0;
        $('nav.page-tabs li a').removeClass('active');
        if (whichContacts === null || whichContacts === undefined || whichContacts === '') {
            $('nav.page-tabs li:eq(0) a').addClass('active');
            //window.location.href = window.location.href + '?contact=home-loan';
        }
        switch (whichContacts) {
            case 'home-loan':
                $('nav.page-tabs li:eq(0) a').addClass('active');
                tabIndex = 0;
                break;
            case 'car-loan':
                $('nav.page-tabs li:eq(1) a').addClass('active');
                tabIndex = 1;
                break;
            case 'personal-loan':
                $('nav.page-tabs li:eq(2) a').addClass('active');
                tabIndex = 2;
                break;
            case 'mortgage-broker':
                $('nav.page-tabs li:eq(3) a').addClass('active');
                tabIndex = 3;
                break;
            case 'enquire-now':
                $('nav.page-tabs li:last-child a').addClass('active');
                tabIndex = $('nav.page-tabs li:last-child').index();
                scrollPage.scrollToSection($('.pepper-form'));
                break;
            default:
                $('nav.page-tabs li:eq(0) a').addClass('active');
                tabIndex = 0;
                break;
        }
        $('ul.page-tabs').removeClass('active');
        $('ul.page-tabs:eq('+tabIndex+')').addClass('active');
        if($('.page-tabs.active').find('.active').length > 0) {
          $.fn.EqualRowHeight('.page-tabs.active', '.featured', 2);    
        }
    }

     $('body').on('click', 'nav.page-tabs a', function(e) {
        e.preventDefault();
        var liIndex = $(this).closest('li').index();
        $('nav.page-tabs a').removeClass('active');
        $(this).addClass('active');
        if ($(this).text().toLowerCase().indexOf('enquire now') != -1) {
            scrollPage.scrollToSection($('.pepper-form'));
        } else {
            $('ul.page-tabs').removeClass('active');
            $('ul.page-tabs:eq(' + liIndex + ')').addClass('active');
             // var getURL = window.location.href.split('?')[0];
            // switch (liIndex) {
            //     case 0:
            //         window.location.href = getURL + '?contact=home-loan';
            //         break;
            //     case 1:
            //         window.location.href = getURL + '?contact=car-loan';
            //         break;
            //     case 2:
            //         window.location.href = getURL + '?contact=finance';
            //         break;

            // }
        }
    });

     $('.scroll-to-page a').on('click', function(e) {
        e.preventDefault();
        scrollPage.scrollToSection();
     });



    });
    
}