var $ = require('../vendors/jquery'),
heightFix = require('../utility/height-fix');

module.exports = function() {


		$('body').on('click', '.rate-tabs .main-tabs a',function(e){
			e.preventDefault();
			var itemNumber = $(this).index();
			//reset
			$('.rate-tabs .main-tabs a').removeClass('active');
			$(this).addClass('active');
			$('.rate-tabs .tabs-swipe, .rate-tabs .prod-header, .rate-tabs .prod-rate').removeClass('active');
			$('.rate-tabs .tabs-swipe:eq('+ itemNumber +'), .rate-tabs .prod-header:eq('+itemNumber+'),.rate-tabs .prod-rate:eq('+ itemNumber+')').addClass('active');
			$('.rate-tabs .prod-header:eq('+itemNumber+') .table-header').removeClass('active');
			$('.rate-tabs .prod-header:eq('+itemNumber+') .table-header:first').addClass('active');
			$('.rate-tabs .prod-rate:eq('+itemNumber+') .rate-table').removeClass('active');
			$('.rate-tabs .prod-rate:eq('+itemNumber+') .rate-table:first').addClass('active');
			//MB-860
			$('.tabs-swipe.active div a').height('auto').heightFix();
			$('.tabs-swipe.active div').height('auto');
		}); //rate-tab main tabs click

		$('body').on('click', '.tabs-swipe a', function(e){
			e.preventDefault();
			var tabIndex = $(this).parent().index();
			var isActive = $(this).hasClass('active');
			if(!isActive) {
				$('').remove
				$('.tabs-swipe.active a, .prod-rate.active .rate-table, .prod-header.active .table-header').removeClass('active');
				$('.prod-header.active .table-header:eq('+ tabIndex+'), .prod-rate.active .rate-table:eq('+tabIndex+')').addClass('active');
				$(this).addClass('active');
			}
		}); //tabs-siwpe tabs click
}