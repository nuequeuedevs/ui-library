var $ = require('../vendors/jquery'),
    checkDevices = require('../utility/check-device'),
    formValidation = require('../utility/form-validation.js');

module.exports = function() {
    var commafields = "";
    commafields = '.currency-mark input, .multi-select .input-field';
    if ($('#pepperShortForm').length > 0) {
        var endShortFormSession = function() {
            var UUID = 'mgnlModelExecutionUUID=' + $("input[name='mgnlModelExecutionUUID']").val();
            var url = $('#pepperShortForm').data('end');
            if (typeof url == 'undefined' || url == null || url == '') {
                url = "/shortform-end-session";
            }
            $.ajax({
                    method: "POST",
                    url: url,
                    data: UUID
                })
                .done(function(data) {
                    if (!$('#pepperShortForm').hasAttr('data-layer')) {
                        s.updateDataLayer(data);
                    }
                    ////console.log("Session ended: ", data);
                })
                .fail(function(data) {
                    ////console.log("Session failed to end: ", data);
                });
        }; //enshort form submission
    } //if short form

    //form validation functions, not calling so putting here for work around
    $.fn.hasAttr = function(attrExists) {
        return (typeof this.attr(attrExists) !== 'undefined' && this.attr(name) !== false);
    };

    function ValidateInput(el) {
        var pattern = el.attr("pattern");
        var isVisible = el.closest('fieldset').is(':visible');
        var isNotChoice = el.closest('fieldset').hasClass('choice-switch');
        if (el.attr("required") && isVisible && !isNotChoice) {
            var val = el.val();
            if (
                (el.is("[type=checkbox]") && !el.is(":checked")) || // Required Checkboxes
                (el.closest("fieldset.date").length == 1 && !formValidation.isValidDate(val)) || // check for valid date
                (el.closest("fieldset.date-picker").length == 1 && !formValidation.isValidDate(val)) ||
                (el.closest("fieldset.validate-age").length == 1 && !formValidation.isValidAge(val)) ||
                (val === "" || val == el.attr("placeholder")) || // Required inputs
                (el.is('select') && el.prop('selectedIndex') == 0) ||
                (el.is("[type=email]") && !formValidation.isValidEmail(val)) ||
                (pattern && !val.match(new RegExp(pattern))) || // If we have a pattern attributes
                el.is("[data-confirm]") && val != $("#" + el.attr("data-confirm")).val()
            ) { // Custom confirmation boxes
                ShowValidationError(el);
                return false;
            } else {
                HideValidationError(el);
                return true;
            }
        } else {
            return true;
        }
    }

    function ValidateAll(el) {
        var pattern = $(el).hasAttr('pattern') ? el.attr("pattern") : 'NA';
        var isRequired = el.hasAttr('required');
        if (isRequired && $(el).is(':not(:disabled)')) {
            switch ($(el).attr('type')) {
                case 'text':
                case 'textarea':
                case 'select':
                    if (pattern != 'NA' && pattern.indexOf('[0-9]') > 0) {
                        return ValidateNumeric(el);
                    } else {
                        return ValidateInput(el);
                    }
                case 'radio':
                case 'checkbox':
                    return ValidateChoice(el.parent());
                    break;
            }
        }
        return true;
    }

    function ValidateNumeric(el) {
        //remove commas before validation.
        var isVisible = el.closest('fieldset').is(':visible');
        if (isVisible) {
            var isCurrencyMark = el.closest('fieldset').hasClass('currency-mark') || el.closest('fieldset').hasClass('multi-select');
            var str = isCurrencyMark ? el.santizeCommas().val() : el.val();
            var numberRegex = /^[]?\d+?([eE][+-]?\d+)?$/;
            if (str.length == 1 && str == '0' && !numberRegex.test(str)) {
                return false;
            }
            var pattrenMinMax;
            var minLength = 0;
            var maxLength = 10;
            if (el.attr('pattern').length > 0) {
                pattrenMinMax = el.attr('pattern').split('{')[1];
                minLength = parseInt(pattrenMinMax.split(',')[0]);
                maxLength = pattrenMinMax.split('}')[0];
                maxLength = parseInt(maxLength.split(',')[1]);
            }

            if (numberRegex.test(str) && str.length >= minLength && str.length <= maxLength) {
                HideValidationError(el);
                //add again.
                if (isCurrencyMark) {
                    el.manageCommas();
                }
                return true;
            } else {
                ShowValidationError(el);
                return false;
            }
        } else {
            return true;
        } //isVisible check
    }

    function ValidateChoice(fieldset) {
        var valid = false;
        var doeshaveRequire = fieldset.find('input[required]').length;
        var extraMargin = ($(window).width() <= 400) ? 50 : 10;
        //var errorHeight = (fieldset.find('p.error').is(':visible')) ? fieldset.find('p.error').outerHeight() + extraMargin : (fieldset.find('p.error').outerHeight()*2) + extraMargin;
        if (doeshaveRequire > 0) {
            fieldset.find('input[required]').each(function() {
                if ($(this).is(':checked')) {
                    valid = true;
                }
            });
        } else {
            valid = true;
        }
        if (!valid) {
            //reset height
            fieldset.find('dl').css('height', 'auto');
            var dlHeight = fieldset.find('dl').outerHeight(true);
            var errorHeight = fieldset.find('p.error').css('display', 'block').outerHeight(true); // quickly display to ensure correct calculation of outerHeight
            fieldset.find('dl').css('height', errorHeight + dlHeight);
            fieldset.find('p.error').css('display', 'none'); // hide again for slide down effect
            fieldset.find('.error').slideDown('fast', function() {
                setFormSlider($('.slides-container'), '0');
            });
        } else {
            fieldset.find('dl').css('height', 'auto');
            fieldset.find('.error').slideUp('fast', function() {
                setFormSlider($('.slides-container'), '0');
            });
        }
        return valid;
    }

    ///for sake for work
    function setFormSlider(el, firstTime) {
        var wrapperWidth = el.parent().innerWidth();
        var totalSlides = el.find('.slide').length;
        var containerWidth = wrapperWidth * totalSlides;
        var wrapperHeight = el.find('.slide.active').outerHeight(true);
        var activeStep = el.find('.slide.active').index() + 1;
        var moveTo = (wrapperWidth * activeStep) - wrapperWidth;
        //set responsive width
        el.outerWidth(containerWidth);
        el.find('.slide').width(wrapperWidth);
        el.parent().height(wrapperHeight);
        if (firstTime != '1') {
            //////console.log('setFormSlider');
            moveSlide(activeStep, -moveTo);
        }
    }


    function ShowValidationError(el) {
        el.css('border-color', '#e00024');
        if (el.hasAttr('type') && el.attr('type') == 'checkbox') {
            el.parent().find('.error').slideDown('fast', function() {
                setFormSlider($('.slides-container'), '0');
            });
        } else {
            el.parent().find('.error').slideDown('fast', function() {
                setFormSlider($('.slides-container'), '0');
            });
        }
        return false;
    }

    function HideValidationError(el) {
        el.css('border-color', '#66CC99');
        if (el.hasAttr('type') && el.attr('type') == 'checkbox') {
            el.parent().find('.error').slideUp('fast', function() {
                setFormSlider($('.slides-container'), '0');
            });
        } else {
            el.parent().find('.error').slideUp('fast', function() {
                setFormSlider($('.slides-container'), '0');
            });
        }
    }

    function Validate(form) {
        valid = true;
        var hasRequired = form.find('input[required]');
        ////console.log('Validation slide inputs' + hasRequired.length);
        //if form contain any required field, then only validate
        if (hasRequired.length > 0) {

            form.find('fieldset.choice-switch:visible').each(function() {
                if (!ValidateChoice($(this))) {
                    valid = false;
                    return false;
                }
            }); //fieldset choice button;

            form.find('fieldset.currency-mark:visible').each(function() {
                $(this).find('input[required]').each(function() {
                    if (!ValidateNumeric($(this))) {
                        valid = false;
                        return false;
                    }
                });
            });

            form.find('.multi-select').each(function() {
                var isMultiSelectChosen = false;
                $(this).find('input[type=checkbox]').each(function() {
                    if ($(this).is(':checked')) {
                        isMultiSelectChosen = true;
                    }
                });
                if (!isMultiSelectChosen) {
                    $(this).find('dd:last-of-type p.error').show();
                    valid = false;
                } else {
                    $(this).find('dd:last-of-type p.error').hide();
                }
            });

            form.find("fieldset:visible:not('.choice-switch')").each(function() {
                if (!$(this).hasClass('currency-mark')) {
                    $(this).find('input[required], textarea[required], select[required]').each(function() {
                        if (($(this).hasAttr('pattern')) && ($(this).attr('pattern').indexOf('0-9') > 0 && $(this).attr('pattern').indexOf('a-z') < 0)) {
                            if (!ValidateNumeric($(this))) {
                                valid = false;
                                return false;
                            }
                        } else {
                            if (!ValidateInput($(this))) {
                                valid = false;
                                return false;
                            }
                        }

                    });
                }
            });
        } else //no require input
        {
            valid = true;
        }
        return valid;
    }

    $("body").on('blur', "input[required]:not('type[radio]'), textarea[required], select[required]", function(e) {
        e.preventDefault();
        if ($(this).closest('fieldset').hasClass('currency-mark') || $(this).closest('fieldset').hasClass('year-month') || ($(this).hasAttr('pattern') && $(this).attr('pattern').indexOf('0-9') > 0 && $(this).attr('pattern').indexOf('a-z') < 0)) {
            ValidateNumeric($(this));
        } else {
            ValidateInput($(this));
        }
    });

    //MB-926
    // $('select[required]').on('change', function(e){
    //     if($(this).prop('selectedIndex') == 0) {
    //         ShowValidationError($(this));
    //     } else {
    //         HideValidationError($(this));
    //     }
    //});

    //generic post function for any form submission without ajax call
    $('body').on('click', "form button[type='submit']", function(e) {
        var form = $(this).parent().parent();
        if (Validate(form)) {
            //$('form').submit(); //Magonlia has multiple form on page.
            return true;
        } else {
            e.preventDefault();
            //console.log('Error: Form is not valid');
        }
    });

    //multi-select MB-758
    $('body').on('change', '.multi-select input[type=checkbox]', function(e) {
        var inputEl = $(this).parent().find('.input-field');
        if ($(this).is(':checked')) {
            $(this).closest('.multi-select').find('dd:last-of-type p.error').hide();
            inputEl.removeAttr('disabled').attr('required', 'required').focus().bind('blur', function(e) {
                ValidateNumeric(inputEl);

            });
        } else {
            inputEl.removeAttr('required').attr('disabled', 'disabled').unbind('blur').blur();
            inputEl.css('border-color', '#c8c8c8').next('.error').hide();
        }
    });


    //conditional drop down MB-2764

    $('body').on('change', 'select.conditional', function() {
        whichField = $('option:selected', this).attr('data-field');
        //hide other associated fields to this drop down
        $('option', this).each(function() {
            var hideOthers = $(this).attr('data-field');
            if (hideOthers != undefined) {
                $('.' + hideOthers).hide();
                //reset value if control is dropdown'
                $('.' + hideOthers).find('select option:eq(0)').prop('selected', true);
            }
        });
        //hide other fields associate to another drop down which appears as choice of this drop down.
        $('select.conditional').each(function() {
            if (!$(this).closest('fieldset').is(':visible')) {
                $('option', this).each(function() {
                    var hideOthers = $(this).attr('data-field');
                    if (hideOthers != undefined) {
                        $('.' + hideOthers).hide();
                    }
                });
            }
        })
        if (whichField != undefined || whichField != null || whichField != '') {
            $('.' + whichField).show();
        }
    });

    //form validation functions ends here;

    function ListenKeyPress() {
        $('.slide.active').focus();
    }

    function setFormSlider(el, firstTime) {
        var wrapperWidth = el.parent().innerWidth();
        var totalSlides = el.find('.slide').length;
        var containerWidth = wrapperWidth * totalSlides;
        var wrapperHeight = el.find('.slide.active').outerHeight(true);
        var activeStep = el.find('.slide.active').index() + 1;
        var moveTo = (wrapperWidth * activeStep) - wrapperWidth;
        //set responsive width
        el.outerWidth(containerWidth);
        el.find('.slide').width(wrapperWidth);
        el.parent().height(wrapperHeight);
        if (firstTime != '1') {
            //////console.log('setFormSlider');
            moveSlide(activeStep, -moveTo);
        }
    }

    function StepSlide(el) {
        var currentSlide = el.closest('.slide.active');
        var isStepBack = el.hasClass('step-back');
        if (!isStepBack) {
            if (Validate(currentSlide)) {
                MoveToNextSlide(el, isStepBack);
            }
        } else if (isStepBack) {
            MoveToNextSlide(el, isStepBack);
        }
    }

    function callPage(type) {
        var wrapperWidth = $('.slide.active').width();
        $('.slides-container .slide').removeClass('active');
        var steps = 0;
        console.log(type);
        switch (type) {
            case 'S':
                steps = $('article.slide').length - 3;
                $(".slide[data-nav='StepFinish']").addClass('active');
                break;
            case 'BS':
                steps = $('article.slide').length - 2;
                $(".slide[data-nav='StepResult']").addClass('active');
                $('.SuccessMessage').show();
                $('.RefMessage').hide();
                $('#NavSteps li a').removeClass('active');
                $('#StepContact').find('a').addClass('active');
                $('.disclaimer-expandable').show();
                break;
            case 'BR':
                steps = $('article.slide').length - 2;
                $(".slide[data-nav='StepResult']").addClass('active');
                $('.SuccessMessage').hide();
                $('.RefMessage').show();
                $('#NavSteps li a').removeClass('active');
                $('#StepContact').find('a').addClass('active');
                $('.disclaimer-expandable').show();
                break;
            case 'R':
                steps = $('article.slide').length - 2;
                $(".slide[data-nav='StepReferral']").addClass('active');
                break;
            case 'T':
                steps = $('article.slide').length - 1;
                $(".slide[data-nav='StepThankYou']").addClass('active');
                break;
        }
        var moveTo = -(wrapperWidth * steps);
        moveSlide(steps, moveTo);
    }

    function moveSlide(step, moveTo) {
        $('.slides-wrapper').css('height', $('.slides-container .slide.active').outerHeight() + 2); //for border
        // Select just Internet Explorer 9
        ////console.log(navigator.userAgent.toLowerCase() + '\n' + '<This is UA');
        ////console.log('Me IE9 ' + isIE9());
        if (checkDevices.isIE9()) {
            $('.slides-container').css('-ms-transform', 'translateX(' + moveTo + 'px)');
        } else {
            $('.slides-container').css('transform', 'translate3d(' + moveTo + 'px, 0px, 0px)');
        }
    }

    function MoveToNextSlide(el, isStepBack) {
        var nextSlide = (isStepBack) ? el.closest('.slide').prev() : el.closest('.slide').next();

        var navIndicator = nextSlide.data('nav');
        var step = el.closest('.slide').index() + 1;
        ////console.log(step);
        var wrapperWidth = el.closest('.slide').width();
        if (el.attr('id') == "getContactDetails2") {
            nextSlide = nextSlide.prev();
            navIndicator = nextSlide.data('nav');
            wrapperWidth = wrapperWidth * 2;
        }
        var slideCurrentPos = $('.slides-container').position().left;
        var moveTo = (isStepBack) ? slideCurrentPos + wrapperWidth : -wrapperWidth * step;
        ////console.log('Wrapper move to first : ' + moveTo);

        if (navIndicator == 'StepStart') {
            $('.disclaimer-expandable').show();
        } else {
            $('.disclaimer-expandable').hide();
        }

        if ($("input[value='Refinance']").is(':checked') && (navIndicator == 'StepPurchaseType' || navIndicator == 'StepLoanType')) {
            nextSlide = (isStepBack) ? nextSlide.prev().prev().prev() : nextSlide.next().next().next();
            navIndicator = nextSlide.data('nav');
            step = (isStepBack) ? step - 4 : step + 3;
            moveTo = (isStepBack) ? slideCurrentPos + (wrapperWidth * 4) : -wrapperWidth * step;
            ////console.log('Wrapper move to second : ' + moveTo);
            $('.slides-container').removeClass('transition-left');
        }

        //Reset If going back from Refinance option slide
        if ($("input[value='Refinance']").is(':checked') && navIndicator == 'StepBorrowers') {
            $("input[value='Refinance']").attr('checked', false);
            var relatedFieldsets = $('.' + $("input[value='Refinance']").attr('name') + '2');
            relatedFieldsets.find('p.error').hide();
            relatedFieldsets.find('input').removeAttr('style');
            relatedFieldsets.hide();
        }
        if (navIndicator == 'StepIncome' || navIndicator == 'StepBorrowers' || $('input[value=Purchase]').is(':checked')) {
            $('.slides-container').addClass('transition-left');
        }
        if (navIndicator == 'StepIncome' || navIndicator == 'StepFinances') {
            switch ($("input[name='borrowerCount']:checked").val()) {
                case '1':
                    nextSlide.find('h2').hide();
                    nextSlide.find('.borrower2').hide();
                    nextSlide.find('.borrower2 input[type=radio]').attr('checked', false);
                    break;
                case '2':
                    nextSlide.find('h2').show();
                    nextSlide.find('.borrower2').show();
                    break;
            }
        } //Income 2nd Borrower
        $('.slides-container .slide').removeClass('active');
        nextSlide.addClass('active');
        if (navIndicator == "StepFinish") {
            //TO DO SPINNER PAGE
            if ($("input[name='creditissue']:checked").val() == 'Yes' && $("input[name='creditissuehappen']:checked").val() == 'Less than Year') {
                step = step + 1;
                moveTo = -moveTo + wrapperWidth;
                moveTo = -moveTo;
                ////console.log('Wrapper move to third : ' + moveTo);
            }
            ////console.log('StepFinish');
            moveSlide(step, moveTo);
        } else {
            ////console.log('MoveToNextSlide herews' + ' ' + moveTo);
            moveSlide(step, moveTo);
        }
        $('#NavSteps li a').removeClass('active');
        $('#' + navIndicator).find('a').addClass('active');


        //backward navigation //MB-2934 STORY: MB-2685
        //MB-2956 REVERT BACK backward nav functionality
        // var totalNavLinks = $('#NavSteps li').length;
        // var activeLinkOrder = $('#NavSteps li').find('a.active').parent().index();
        // var enableBackLinks =  -((totalNavLinks - activeLinkOrder) - totalNavLinks);
        // for (i = 0; i < enableBackLinks; i++) {
        //     $('#NavSteps li:eq('+i+')').find('a').addClass('back-link');
        //     $('#NavSteps li:eq('+(i+1)+')').find('a').addClass('back-link');
        // }
        //backward functionality end here.

        if ($(window).width() <= 768) {
            if ($('.form-steps>h1').length <= -1) {
                $('.form-steps').add('h1');
            }
            $('.form-steps>h1').html($('#' + navIndicator).find('span').text())
                .css('font-size', '1.0rem')
                .css('text-align', 'center')
                .css('padding-top', '0px')
                .show();
        }

        $(window).scrollTop(0);

        //only on qatest.
        //if(window.location.href.indexOf('local') < 0) {
        var currentSlide = el.closest('.slide');
        $(commafields).each(function() {
            $(this).santizeCommas();
            //for testing only
            //console.log($(this).val() + '\n');
        });
        var formData = $('#pepperShortForm').serialize();
        var stepAction = isStepBack;
        var values = {
            shortFormData: formData,
            stepBackAction: stepAction,
            stepId: navIndicator,
            stepNumber: step
        };
        //////console.log(formData);

        var url = "/analytic-event/*";
        $.ajax({
                method: "POST",
                url: url,
                data: values,
                async: true
            })
            .done(function(data) {
                if (!$('#pepperShortForm').hasAttr('data-layer')) {
                    s.updateDataLayer(data);
                }
                $('.currency-mark input').each(function() {
                    $(this).manageCommas();
                    //for testing only
                    //console.log($(this).val() + '\n');
                });
                //$('.slide.active').find('input:visible:first').focus();
            })
            .fail(function(data) {
                //$('.slide.active').find('input:visible:first').focus();
            });
        //}
    } //ShorForm next slider function



    //Short Form Next Previous
    $('body .step-next, .step-back').on('click', function(e) {
        e.preventDefault();
        StepSlide($(this));
    });


    $('body .step-next').on('keydown', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode == 9 && e.shiftKey) {} else if (keyCode == 13) {
            e.preventDefault();
            StepSlide($(this));
        }
    });

    $('body .step-back').on('keydown', function(e) {
        var keyCode = e.keyCode || e.which;
        var inputEl = $(this).closest('.slide.active').find('input:visible');
        var choiceEl = $(this).closest('.slide.active').find("span[tabindex='1']:visible");
        var slideHasSlider = $('.slide.active').find('#js-display-change');
        if (keyCode == 9 && e.shiftKey) {
            e.preventDefault();
            if (inputEl.length > 0) {
                inputEl.focus();
            } else if (choiceEl.length > 0) {
                choiceEl.focus();
            } else if (slideHasSlider.length > 0) {
                slideHasSlider.focus();
            }
        } else if (keyCode == 9) {}
    });

    //MB-2956 REVERT BACK backward nav functionality

    // $('body').on('click', '.back-link',function(e){
    //     var slideToShow = $(".slide[data-nav="+$(this).parent().attr('id')+"]");
    //     var slidetoShowPosition = slideToShow.position().left;
    //     // console.log(activeSlide);
    //     // console.log(slidePosition);
    //     //console.log(slidetoShowPosition);
    //     $('#NavSteps li a').removeClass('active');
    //     $('.slides-container .slide').removeClass('active');
    //     slideToShow.addClass('active');
    //     $(this).addClass('active');
    //     moveSlide(slideToShow.index(), -slidetoShowPosition);

    // });

    $("body .currency-mark input[required]").on('keydown', function(e) {
        var str = $(this).val();
        var keyCode = e.keyCode || e.which;
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        var numberRegex = /^[+-]?\d+?([eE][+-]?\d+)?$/;
        if (str.length == 1 && (str.substr(0, 1) == '0' || !numberRegex.test(str))) {
            e.preventDefault();
        }
        // if(str.length == 1 && (e.keyCode == 96 || e.keyCode == 48) {
        //     e.preventDefault();
        // }
    });

    $("body .currency-mark input, body .multi-select .input-field").on('focus keyup', function(e) {
        $(this).manageCommas();
    });

    //slide form wizard
    $(document).ready(function() {
        //GCSE V2 Changes after excluding API change.
        // if (window.location.href.indexOf('queryStr') > 0) {
        //     try {
        //         digitalData.events.push({
        //             "type": "analytics",
        //             "event": "internalSearch",
        //             "searchTerm": getParameterByName(queryStr),
        //             "searchResultCount": 0 //Couldn't find where is count in Google CSE API V2
        //             //Api link
        //             //https://developers.google.com/custom-search/docs/element
        //         });
        //     } catch (e) {}
        // }
        if ($('body .short-form').length > 0) {
            $('.slide').first().find('p').first().addClass('para-hero').find('em').css({
                'font-size': '0.8rem'
            });
            setFormSlider($('.slides-container'), '1');
            $(window).resize(function() {
                setFormSlider($('.slides-container'), '0');
            });
        }
        //MB-3079, MB-3081
        //Execute for 
        if ($('div.Form__MainBody').length) {
            //each choice switch on page
            $('.choice-switch').each(function() {
                //each option of choice switch
                if ($(this).next().is('input:hidden') && $(this).next().val().toLowerCase().indexOf("end") < 0) {
                    //Choice buttons, which has conditional fields
                    var thisSwitch = $(this);
                    //loop now all options
                    $(this).find('input[type=radio]').each(function(i) {
                        var fieldName = $(this).attr('name');
                        var fieldIndex = i + 1;
                        var relatedHiddenFields = thisSwitch.nextUntil("input:hidden[value*=Ends]");
                        var targetedHidden;
                        //loop now all hidden fields within fieldset options
                        relatedHiddenFields.each(function() {
                            //find the targeted hidden field which has options fieldset
                            if ($(this).is('input:hidden') && $(this).val().indexOf(fieldIndex) > 0) {
                                targetedHidden = $(this);
                            }

                            if (targetedHidden != undefined || targetedHidden != null) {
                                var conditionalFields = targetedHidden.nextUntil("input[type=hidden]");
                                conditionalFields.each(function() {
                                    if ($(this).is('fieldset')) {
                                        $(this).addClass(fieldName + fieldIndex).hide();
                                    }
                                });
                            }
                        }); //each option loop;
                    }); //each hidden loop
                }
            }); //each switch loop;
            //End of choice switch hack for epi.
            //Email to sent placeholder
            $('select.conditional').on('change', function(e) {
                if ($(this).val().indexOf('@') > 0) {
                    e.preventDefault();
                    $('input:hidden[value*=emailtosent]').val($(this).val());
                }
            });
        }
    });

    $('body .start-again').on('click', function(e) {
        $('.slides-container .slide').removeClass('active');
        $('.slides-container .slide:first').addClass('active');
        moveSlide(1, 0);
    });

    $(document).ajaxComplete(function() {
        // function callPage(type) {
        //     var wrapperWidth = $('.slide.active').width();
        //     $('.slides-container .slide').removeClass('active');
        //     var steps = 0;
        //     ////console.log('ajaxComplete');
        //     switch (type) {
        //         case 'S':
        //             steps = $('article.slide').length - 3;
        //             $(".slide[data-nav='StepFinish']").addClass('active');
        //             break;
        //         case 'R':
        //             steps = $('article.slide').length - 2;
        //             $(".slide[data-nav='StepReferral']").addClass('active');
        //             break;
        //         case 'T':
        //             steps = $('article.slide').length - 1;
        //             $(".slide[data-nav='StepThankYou']").addClass('active');
        //             break;
        //     }
        //     var moveTo = -(wrapperWidth * steps);
        //     moveSlide(steps, moveTo);
        // }
    });
    var isReferal = false;
    if ($('body #pepperShortForm').length > 0) {
        ListenKeyPress();
        $('main.content').css('background-color', '#eee');
        $("#shortformSubmit").on("click", function(e) {

            e.preventDefault();
            if (Validate($(this).closest('.slide'))) {

                $('.page-loader').show();
                $(window).scrollTop(0);
                //remove commas before sending
                $('.currency-mark input').each(function() {
                    $(this).santizeCommas();
                    //for testing only
                    //console.log($(this).val() + '\n');
                });
                var formData = $('#pepperShortForm').serialize();
                var url = $('#pepperShortForm').data('url');
                if (typeof url == 'undefined' || url == null || url == '') {
                    url = "/shortform-approval-tool";
                }
                $.ajax({
                    method: "POST",
                    url: url,
                    data: formData,
                    statusCode: {
                        404: function() {
                            //alert('Me here on first servelet 404 error');
                            callPage('T');
                            $('.page-loader').hide();
                            $(window).scrollTop(0);
                            endShortFormSession();
                        }
                    }
                }).done(function(data) {
                    var Variation = $('input[name=shortFormPageType]').val();
                    if (!$('#pepperShortForm').hasAttr('data-layer')) {
                        s.updateDataLayer(data);
                    }
                    if (data.status === "success") {
                        if (data.Qualify === "True") {
                            var email = $('input[name=email]').val();
                            $('#shortformResultAmount').html(data.Amount);
                            $('#shortformResultEmail').html(email);
                            $('#shortformResultId').html(data.nextGenDocID);
                            $('.page-loader').hide();
                            var sendEmailto = ($('#pepperShortForm').hasAttr('data-email')) ? $('#pepperShortForm').attr('data-email') : '/shortform-send-email';
                                $.ajax({
                                    method: "POST",
                                    url: sendEmailto,
                                    data: formData + '&Amount=' + $('#shortformResultAmount').text(),
                                    async: true
                            });
                            if (Variation != 'AB-variation-1') {
                                callPage('S');
                                endShortFormSession();
                            } else {
                                //it is variant B
                                callPage('BS');
                            }
                        } else if (data.Qualify === "False") {
                            if(Variation != 'AB-variation-1') {
                                callPage('R');
                            } else {
                                isReferal = true;
                                callPage('BR');
                            }
                            $('.page-loader').hide();
                            $(window).scrollTop(0);
                            endShortFormSession();
                        }
                    } else if (data.status === "failed") {
                        callPage('R');
                        $('.page-loader').hide();
                        $(window).scrollTop(0);
                        endShortFormSession();
                    } else {
                        callPage('T');
                        $('.page-loader').hide();
                        $(window).scrollTop(0);
                        endShortFormSession();
                    }
                }).fail(function(jqXHR, textStatus, error) {
                    alert('Failed ajax call ' + error + '\n' + textStatus + '\n' + jqXHR.status);
                    callPage('T');
                    $('.page-loader').hide();
                    $(window).scrollTop(0);
                    endShortFormSession();
                });
            }
        }); //short form call, both variaitions
        //new contact us page call
         var isVariantRef = false;
        $('body').on('click', '#getContactDetails, #getContactDetails2', function(e) {
            isVariantRef = ($(this).attr('id') == 'getContactDetails2') ? true : false;
            MoveToNextSlide($(this), true);
        });

        $('body').on('click', '#shortformSubmitContactDetail, #shortformConsolidateSubmit', function(e) {
            if($(this).attr('id') == 'shortformConsolidateSubmit' && isReferal){
                isVariantRef = true;
            }
            e.preventDefault();
            if (Validate($(this).closest('.slide'))) {
                $('.page-loader').show();
                $(window).scrollTop(0);
                //remove commas before sending
                $('.currency-mark input').each(function() {
                    $(this).santizeCommas();
                    //for testing only
                    //console.log($(this).val() + '\n');
                });
                var formData = $('#pepperShortForm').serialize();
                var url = "/shortform-contact-details";
                var redirect = isVariantRef ? '/home-loan-pre-approval-tool1/enquiry-thank-you' : $(this).attr('data-redirect');
                var resultAmount;
                $.ajax({
                    method: "POST",
                    url: url,
                    data: formData,
                    statusCode: {
                        404: function() {
                            console.log('No Servelt found');
                            callPage('T');
                            $('.page-loader').hide();
                            $(window).scrollTop(0);
                            endShortFormSession();
                        }
                    }
                }).done(function(data) {
                    // console.log(data.status);
                    // console.log(data.Qualify);
                    $('.page-loader').hide();
                    endShortFormSession();
                    //MB-3020
                    if (data.status === "success") {
                        if (data.Qualify === "True") {
                            var sendEmailto = ($('#pepperShortForm').hasAttr('data-email')) ? $('#pepperShortForm').attr('data-email') : '/shortform-send-email';
                            $.ajax({
                                method: "POST",
                                url: sendEmailto,
                                data: formData + '&Amount=' + $('#shortformResultAmount').text()
                            }).done(function() {
                                window.location.href = redirect;
                            }).fail(function(jqXHR, textStatus, error) {
                                //alert('Failed ajax call ' + error + '\n' + textStatus + '\n' + jqXHR.status);
                                window.location.href = redirect;
                            });
                        } else {
                            window.location.href = redirect;
                        }
                    } else {
                        callPage('T');
                        $('.page-loader').hide();
                        $(window).scrollTop(0);
                    }
                }).fail(function(data) {
                    callPage('T');
                    $('.page-loader').hide();
                    $(window).scrollTop(0);
                    endShortFormSession();
                });
            }
        });
    } // form submission

    //Switch Button Show Hide Relevant Fields
    $("body").on('change', '.choice-switch input[type=radio], .choice-switch input[type=checkbox]', function(e) {
        e.preventDefault();
        var fiedlsetClassName = $(this).attr('name');
        var showFieldsets = fiedlsetClassName + ($(this).parent().index() + 1);
        var howmanyOptions = $(this).parent().parent().find('label');
        howmanyOptions.each(function(index) {
            //first hide all option
            var hideThis = fiedlsetClassName + (index + 1);
            $('.' + hideThis).find('input[type=radio], input[type=checkbox]').attr('checked', false);
            $('.' + hideThis).find('input:not([type=radio], [type=checkbox])').val('');
            $('.' + hideThis).hide();
        });
        //then display this option
        $('.' + showFieldsets).show();
        var checkBorrow = parseInt(checkDevices.getParameterByName('borrow'));
        if (checkBorrow > 0) {
            $("#refinanceAmount").val(checkBorrow);
        }
        $(this).parent().parent().find('p.error').slideUp('fast');
        $(this).parent().parent().parent().css('height', 'auto');
        //now reset form height;
        if ($(this).attr('value') == 'Self Employed' || $(this).attr('value') == 'Self-Employed') {
            $('.' + showFieldsets).find("input[value='Yearly']").prop('checked', true).parent().parent().find('p.error').hide();
        }
        if ($('#directContactUs').length > 0) {
            if ($(this).attr('value') == 'Email') {
                $("#directContactUs input[name='email']").attr('required', 'required').bind('blur', function(e) {
                    e.preventDefault();
                    ValidateInput($(this));
                });
                $("#directSqueezeForm input[name='phoneNo']").removeAttr('required');
                $("#directSqueezeForm input[name='phoneNo']").unbind('blur');
            } else if ($(this).attr('value') == 'Telephone') {
                // $("#directContactUs input[name='email']").removeAttr('required');
                // $("#directContactUs input[name='email']").unbind('blur');
                $("#directSqueezeForm input[name='phoneNo']").attr('required', 'required').bind('blur', function(e) {
                    e.preventDefault();
                    ValidateNumeric($(this));
                });
            }
        } else {
            if ($(this).attr('value') == 'Telephone') {
                $('#telephone').attr('required', 'required').attr('pattern', '[0-9+]{6,14}').focus();
                $('#telephone').bind('blur', function(e) {
                    e.preventDefault();
                    ValidateNumeric($(this));
                });
            } else if ($(this).attr('value') == 'Email' || $(this).attr('value') == 'No Contact') {
                $('#telephone').removeAttr('required').removeAttr('pattern');
                $('#telephone').unbind('blur').css('border', '1px solid #c8c8c8').parent().find('p.error').slideUp('fast');
            }
            setFormSlider($('.slides-container'), '0');
        }

    });

    //contact us form submission
    $('body').on('click', '#directContactUs button.submit', function(e) {
        e.preventDefault();
        //Tick to receive Pepper news or offers
        if (Validate($('#directContactUs'))) {
            try {
                digitalData.events.push({
                    "type": "analytics",
                    "event": "submitContactUs",
                    "firstName": $('#firstName').val(),
                    "lastName": $('#lastName').val(),
                    "email": $('#email').val(),
                    "phoneNo": $('#phoneNo').val(),
                    "preferredContact": $("input[name='preferredContact']:checked").val(),
                    "enquiryTypes": $("select[name='enquiryTypes']").val(),
                    "receivePepperNews": ($("input[name='receivePepperNews']").is(':checked')) ? "Tick to receive Pepper news or offers" : ""
                });
            } catch (e) {
                console.log(e.message);
                //regardless analytics passed or failed, finally it should submit the form.
            } finally {

                $('.page-loader').show();
                $(window).scrollTop(0);
                var formData = $('#directContactUs').serialize();
                var url = "/direct-form-contact-us";
                $.ajax({
                        method: "POST",
                        url: url,
                        data: formData
                    })
                    .done(function(data) {
                        //////console.log("Data Saved: ", data);
                        if (data.status === "success") {
                            $('.page-loader').hide();
                            $('#directContactUs').hide();
                            window.location.href = 'contact-us/enquiry-submitted';
                            //$('.pepper-form p').addClass('centre-block');
                        } else {
                            ////console.log('Form Submitted UNSUCCESSFULl');
                            $('.page-loader').hide();
                        }
                    })
                    .fail(function(data) {
                        ////console.log('Unable to send the enquiry, please try again later');
                        $('.page-loader').hide();
                    });
            } //finally
        } //if

    });
    //contact us form submission

    //Side Nav

    //radio button functionality on keypress as per WCAG LEVEL AA
    $('body').on('keypress', '.choice-switch span', function(e) {
        e.preventDefault();
        var code = e.which;
        if (code == 13 || code == 32) {
            $(this).prev().prop("checked", true).trigger("change");
        }
    });

    //initiate short form


    $("body").on('click', '.cta-event', function(e) {
        e.preventDefault();
        var sendValue = $(this).data('analytics');
        var target = $(this).hasAttr('target') ? $(this).attr('target') : '';
        try {
            digitalData.events.push(sendValue);
        } catch (e) {} finally {
            //if Shortform slider
            if ($(this).hasClass('call-shortform')) {
                var noSlider = $(this).closest('.shortform-slider').hasClass('no-slider');
                var howMuch = (noSlider) ? 0 : $('#js-display-change').val();
                var minVal = (noSlider) ? 0 : parseInt($('#js-display-change').data('min'));
                if (howMuch >= minVal) {
                    $('.slide-error:visible').slideUp();
                    var pageURL;
                    if (location.href.indexOf('3000') != -1) {
                        pageURL = (noSlider) ? 'short-form.html' : 'short-form.html?borrow=' + howMuch;
                        window.location = pageURL;
                    } else {
                        pageURL = window.location.href.split('.au')[0];
                        pageURL = (noSlider) ? $(this).attr('href') : $(this).attr('href') + '?borrow=' + howMuch;
                        window.open(pageURL);
                    }
                } else {
                    $('.slide-error').slideDown();
                }
            } else {
                var gotoPage = (window.location.href.indexOf('car-loans') > 0) ? $(this).attr('href') + '?contact=car-loan' : $(this).attr('href') + '?contact=enquire-now';
                if (target == '_blank') {
                    window.open($(this).attr('href'));
                } else if (sendValue == "enquiryForm" || sendValue == "enquiryFormCta") {
                    if (window.location.href.indexOf('car-loans') > 0) {
                        window.location.href = gotoPage;
                    } else {
                        window.location.href = gotoPage;
                    }
                } else {
                    window.location.href = $(this).attr('href');
                }
            }
        }
    });

    $('body').on('click', '.campaign-enquiry button.submit', function(e) {
        e.preventDefault();
        var campaignForm = $(this).closest('form');
        var formId = campaignForm.attr('id');
        var submitTo = campaignForm.attr('data-url');
        var redirectTo = campaignForm.attr('data-redirect');
        var eventName = campaignForm.attr('data-eventname');

        if (Validate($('#' + formId))) {
            $('.page-loader').show().css('display', 'block');
            //sanitize commas from input fields
            $(commafields).each(function() {
                $(this).santizeCommas();
                //console.log($(this).val());
            });

            //send analytics
            if ($("form").data('analytic') == 'true' || $("form").data('analytic') == true) {
                try {
                    if (location.href.indexOf('contact-us') > 0) {
                        //contact us analytics
                        digitalData.events.push({
                            "type": "analytics",
                            "event": eventName,
                            "firstName": $('#firstName').val(),
                            "lastName": $('#lastName').val(),
                            "email": $('#email').val(),
                            "phoneNo": $('#phoneNo').val(),
                            "preferredContact": $("input[name='preferredContact']:checked").val(),
                            "enquiryTypes": $("select[name='enquiryTypes']").val(),
                            "receivePepperNews": ($("input[name='receivePepperNews']").is(':checked')) ? "Tick to receive Pepper news or offers" : ""
                        });
                    } ////analytics for contact-us MB-733.
                    else if (location.href.indexOf('credit-savvy') > 0) {
                        //credit savvy
                        switch ($("input[name='iam']:checked").val()) {
                            case 'A Home Buyer':
                                redirectTo = '/credit-savvy/thank-you';
                                break;
                            case 'An Investor':
                                redirectTo = '/credit-savvy/thank-you-invest';
                                break;
                            case 'A Refinancer':
                                redirectTo = '/credit-savvy/thank-you-refinance';
                                break;
                            case 'Self Employed ':
                                redirectTo = '/credit-savvy/thank-you-self-employed';
                                break;
                            case 'Self Employed':
                                redirectTo = '/credit-savvy/thank-you-self-employed';
                                break;
                        }
                        digitalData.events.push({
                            "type": "analytics",
                            "event": eventName,
                            "segment": $("input[name='iam']:checked").val(),
                            "marketingChannel": $("#marketingChannel").val(),
                            "email": $('#email').val()
                        });
                    } //analytics for credit-savvy.
                    else {
                        //for all other campaigns
                        digitalData.events.push({
                            "type": "analytics",
                            "event": eventName,
                            "marketingChannel": $("#marketingChannel").val(),
                            "email": $('#email').val()
                        });
                    } //analytics for all other campaigns.
                } catch (e) {

                }
            } //if send data-analytics true

            var formData, contentType;
            var processData = false;
            if (campaignForm.find('input[type=file]').length > 0) {
                //$(this)[0]
                formData = new FormData();
                $('input[type=file]').each(function(i) {
                    formData.append('file' + (i + 1), document.getElementById('Files' + (i + 1)).files[0]);
                });
                $('input:not([type=file]), select, textarea').each(function() {
                    formData.append($(this).attr('name'), $(this).val());
                });
                contentType = false;
                processData = false;
            } else {
                formData = $('#' + formId).serialize();
                contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
                processData = true;
            }

            $.ajax({
                    method: "POST",
                    cache: false,
                    url: submitTo,
                    data: formData,
                    contentType: contentType,
                    processData: processData,
                    enctype: 'multipart/form-data',
                    async: false
                })
                .done(function(data) {
                    //////console.log("Data Saved: ", data);
                    //s.updateDataLayer(data);
                    if (data.status === "success") {
                        //$('.page-loader').hide();
                        $(window).scrollTop(0);
                        $('#directContactUs').hide();
                        if (location.href.indexOf('panthers-win') > 0) {
                            $.ajax({
                                method: 'POST',
                                url: "https://app-3QMUE9C46K.marketingautomation.services/webforms/receivePostback/MzawMDE2MjI1AgA/e1ab5253-cbaa-4b56-b8cd-8bc9f2b8ded4/jsonp/?firstname=" + $('#firstName').val() + "&lastname=" + $('#lastName').val() + "&" + "email=" + $('#email').val() + "&" + "phone=" + $('#phoneNo').val() + "&" + "subscribe=" + $('#toReceiveMoreInformationFromPepper').val()
                            });
                        }
                        window.location.href = redirectTo;
                        //$('.pepper-form p').addClass('centre-block');
                    } else if (data.status === "module-test-failed") {
                        $('.server-errors header h4').html(data.MessageFailedTile);
                        $('.server-errors header p').html(data.MessageFailed);
                        $('.server-errors ul li').html(data.MessageFailedAnswerIncorrectly);
                        $('.server-errors').show();
                        if (data.MessageFailedAnswerIncorrectly == undefined ||
                            data.MessageFailedAnswerIncorrectly == '' ||
                            data.MessageFailedAnswerIncorrectly == null) {
                            $('.server-errors ul').hide();
                        }
                        $('.page-loader').hide();
                    } else {
                        ////console.log('Form Submitted UNSUCCESSFULl');
                        $('.page-loader').hide();
                    }
                })
                .fail(function(data) {
                    ////console.log('Unable to send the enquiry, please try again later');
                    $('.page-loader').hide();
                    if (submitTo.toLowerCase().indexOf('broker-forgotpassword') > 0) {
                        $('input[name=username] ~ p.error span').html(
                            'Your username is the email address that you have registered with us. ' +
                            'We could not recognise this username, to let us assist you please give us a call on ' +
                            '<a href="tel:1800737737">1800 737 737</a>.');
                        $('input[name=username] ~ p.error').show();
                    } else {
                        //alert("An error has ocurred. Please try again later");
                        $('.error-alert').remove();
                        var emailError = '<aside class="server-errors error-alert"><ul><li class="form-warn">An error has ocurred. Please try again later</li></ul></aside>';
                        $('#' + formId).append(emailError);
                        setTimeout(function() {
                            //console.log($('.email-error')); 
                            $('.error-alert').remove();
                        }, 8000);

                        $.ajax({
                                method: "POST",
                                url: "/pepper-alert?exceptionURL=" + location.href,
                            })
                            .done(function(data) {
                                //console.log("email sent");
                            })
                            .fail(function(data) {
                                //console.log("email not sent");
                            });
                    }
                });
        } //VALIDATE FORM FIRST
        else {
            //only for mobiles
            if (($(window).width() <= 640)) {
                //scroll to the error in the form
                var scrollTo = $('#' + formId).find('.error:visible:first').closest('fieldset').offset().top - 60;
                $("html, body").animate({
                    scrollTop: scrollTo
                }, 500);
            }

        }
    });

    $('body').on('click', '.submit-form', function(e) {
        e.preventDefault();
        if ($(this).hasClass('search-submit-form')) {
            SendAnalytics($(this));
        }
        $(this).closest('form').submit();
    });

    $('body').on('keydown', '#DatePicker', function(e) {
        formValidation.preventTypeInNonNum(e);
        if (e.keyCode != 8) { //to allow user to use backspace
            formValidation.dateMask("/", $(this));
        }
    });
}