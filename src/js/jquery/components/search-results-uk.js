var $ = require('../vendors/jquery'),
    checkDevices = require('../utility/check-device');

module.exports = function() {};

google.load('search', '1', {language: 'en'});
google.setOnLoadCallback(function() {
  var customSearchOptions = {};
  var customSearchControl =   new google.search.CustomSearchControl('013781352769439467301:bwvzcq4-en8', customSearchOptions);
  customSearchControl.setResultSetSize(10);
  var options = new google.search.DrawOptions();
  options.setAutoComplete(true);
  customSearchControl.setSearchCompleteCallback(null,function() { searchCompleteCallback(customSearchControl) });
  customSearchControl.draw('customGoogleSearchDIV', options);
  // Run a query
  customSearchControl.execute(checkDevices.getParameterByName('queryStr'));
}, true);

function searchCompleteCallback(customSearchControl) {
  //console.log(customSearchControl)
  var r = /\d/;
  var s = customSearchControl.Ig.innerText;
  var count = s.match(r)? s.match(r)[0] : 0;
  var actualSearchedText = $(".gs-spelling-original a b i").length > 0 ? $(".gs-spelling a b i").html() : customSearchControl.gf;

  try {
    digitalData.events.push({
      "type": "analytics",
      "event": "internalSearch",
      "searchTerm": actualSearchedText,
      "searchResultCount": count
    });
  } catch (e) {}
}