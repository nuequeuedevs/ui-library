var $ = require('../vendors/jquery');

module.exports = function() {
    function PhoneNumberAnalytics(CallType) {
        try {
            digitalData.events.push(CallType);
            mmConversionTag(612424, this);
        } catch (e) {}
    }

    $('body').on('click', 'a.tel-analytic', function() {
        var CallType = $(this).data('analytics');
        try {
            digitalData.events.push(CallType);
            mmConversionTag(612424, this);
        } catch (e) {
            console.log(e.Message);
        }
    });


    //Side nav analtyics
    $("body").on('click', '.side-nav a', function(e) {

        if ($(this).attr('href').indexOf('tel:') >= 0) {
            PhoneNumberAnalytics('call137377Sticky');
        } else {
            e.preventDefault();
            if ($(this).attr('href').indexOf('contact-us') > 0) {
                try {
                    digitalData.events.push("enquiryFormSticky");
                } catch (e) {} finally {
                    if($(this).attr('href').indexOf('?contact') > 0) {
                        window.location.href = $(this).attr('href');
                    } else {
                        window.location.href = $(this).attr('href') + '?contact=enquire-now';    
                    }
                }
            } else if ($(this).attr('href').indexOf('pre-approval') > 0) {
                try {
                    digitalData.events.push("preApprovalToolSticky");
                    window.location.href = $(this).attr('href');
                } catch (e) {}
            } else {
                //just treat as link NO ANALYTICS
                window.location.href = $(this).attr('href');
            }
        }
    });

    //lsn-login call analytics
    $('body').on('click', '.lsn-login', function(e){
    	e.preventDefault();
        digitalData.events.push("onlineBanking");
        window.open($(this).attr('href'));
    });

    // $("a[href*='tel:'], a[href*='Tel:']").on('click', function() {
    //     if ($(this).parent().hasClass('side-nav')) {
    //     } 
    //     // else if ($(this).parent().hasClass('call-now')) {
    //     //     //PhoneNumberAnalytics('call137377Header');
    //     // } else {
    //     //     PhoneNumberAnalytics('call137377');
    //     // }
    // });

    $('body').on('click', '#AddItem', function(e){
    e.preventDefault();
    var component = $('#ChooseComponent').val();
    var GridColumn;
    $("input[name='GridColumns']").each(function(){
        if($(this).is(':checked')){
            GridColumn = $(this).attr('value');
        }
    });
    $('.hidden').find('.'+component).clone().addClass(GridColumn).appendTo('.epi-layout');
  });

  $('body').on('click', '#RefreshLayout', function(e){
      $('.epi-layout').html('');
  });

  $('body').on('click', '#AddPortholeItem', function(e){
    e.preventDefault();
    var addItemto = $('body .layout').find('.container');
    addItemto.append(addItemto.html());
  })

  $('body').on('click', '.add-right', function(e) {
    e.preventDefault();
    $(this).closest('p').toggleClass('add-right-padding');
  });
  $('body').on('click', '.add-left', function(e) {
    e.preventDefault();
    $(this).closest('p').toggleClass('add-left-padding');
  });
  $('body').on('click', '.add-top', function(e) {
    e.preventDefault();
    $(this).closest('p').toggleClass('add-top-padding');
  });
  $('body').on('click', '.add-bottom', function(e) {
    e.preventDefault();
    $(this).closest('p').toggleClass('add-bottom-padding');
  });
}