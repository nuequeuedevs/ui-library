var $ = require('../vendors/jquery'),
Cookies = require('js-cookie');
 
module.exports = function() {
  $(document).ready(function(){
    var noticeVersion;
    if($('.notice').length) {
      noticeVersion = $('.notice').attr('data-version')
      CookieNotice(noticeVersion);
    }
    $('body').on('click', '.notice .close-bt', function(e) {
      e.preventDefault();
      Cookies.set('pepper-notice', 'seen'+ $('.notice').attr('data-version'), { expires: 365, path: '/' });
      CookieNotice(noticeVersion);
    });
  });

  function CookieNotice(noticeVersion) {
    if (Cookies.get("pepper-notice") == "seen"+noticeVersion) {
              $(".slidedown-modal").addClass('shown');
      } 
      else {
        $(".slidedown-modal").removeClass('shown');
      }
  }
}