var $ = jQuery = require('../vendors/jquery'),
    slick = require('../vendors/slick');
module.exports = function() {
    //Carousel-container
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                }
            }, {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object 
        ]
    });



    // Carousel-circle
    $('.circle-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        responsive: [{
            breakpoint: 1024,
            settings: "unslick"
        }, {
            breakpoint: 640,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    // Carousel-gallery
    $('body .gallery-slider').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        centerMode: false,
        responsive: [{
            breakpoint: 960,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 640,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });


    //swipe-cards
    $('body .swipe-cards:not(.no-swipe)').each(function() {
        var delay = ($(this).data('seconds-per-slide')) ? ($(this).data('seconds-per-slide') * 1000) : 5000; // milli seconds

        $(this).find('.card-list').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            dots: true,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: delay,
            responsive: [{
                breakpoint: 960,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            }, {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    });

    $(document).ready(function() {
        setTimeout(function() {
            var stHeight = $('.swipe-cards .slick-track').outerHeight();
            $('.swipe-cards .slick-slide').css('height', stHeight + 'px');
            $('.swipe-cards .slick-slide').css('background-color', '#fff');
        }, 500);
    });



    //slick responsive issue
    var $responsiveSlider = $('.has-swipe:not(.desktop-swipe) .swipe-slider');
    var $responsiveSliderDesktop = $('.desktop-swipe:not(.three-columns) .swipe-slider');
    //MB-2105
    var $heroCarousel = $('.hero-carousel');
    //MB-2036
    var $responsiveSliderItems = $responsiveSliderDesktop.find('.porthole').length;
    var $responsiveSlideOnDesktop = $responsiveSliderDesktop.parent().hasClass('desktop-swipe');
    var autoSlide = ($('.desktop-swipe .swipe-slider').attr('data-autoSlide') == 'true') ? true : false;
    var slideDelay = parseInt($('.desktop-swipe .swipe-slider').attr('data-delay'));
    slideDelay = (slideDelay > 0) ? slideDelay * 1000 : 5000;
    var showSliderScreen = function($widthScreen) {
            //MB-2105
            if ($heroCarousel.length) {
                var isAuto = ($heroCarousel.attr('data-autoPlay') == 'true') ? true : false;
                var speedDelay = parseInt($heroCarousel.attr('data-delay'));
                speedDelay = (speedDelay > 0) ? speedDelay * 1000 : 5000;
                $heroCarousel.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    focusOnSelect: true,
                    lazyLoad: 'ondemand',
                    centerMode: false,
                    dots: true,
                    arrows: false,
                    mobileFirst: false,
                    autoplay: isAuto,
                    pauseOnHover: true,
                    autoplaySpeed: speedDelay,
                    fade: true
                }); //slick hero carousel it.
            } //hero carousel //MB-2105

            if ($widthScreen <= "480") {
                if (!$responsiveSlider.hasClass('slick-initialized')) {
                    $responsiveSlider.slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        focusOnSelect: true,
                        lazyLoad: 'ondemand',
                        centerMode: false,
                        dots: true,
                        mobileFirst: true,
                        autoplay: autoSlide,
                        autoplaySpeed: speedDelay
                    }); //slick it.
                } //slick has no initialization 
                if($responsiveSlideOnDesktop && $responsiveSliderDesktop.hasClass('slick-initialized')) {
                   $responsiveSliderDesktop.slick('unslick'); 
                }
                if ($responsiveSlideOnDesktop && $responsiveSliderItems > 4) {
                        $responsiveSliderDesktop.slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: autoSlide,
                            focusOnSelect: true,
                            lazyLoad: 'ondemand',
                            centerMode: false,
                            dots: true,
                            autoplay: autoSlide,
                            autoplaySpeed: slideDelay,
                            mobileFirst: false
                        }); //slick it.
                }
            } else {
                //items must be 8 no more no less
                if($responsiveSlideOnDesktop && $responsiveSliderDesktop.hasClass('slick-initialized')) {
                   $responsiveSliderDesktop.slick('unslick'); 
                }
                if ($responsiveSlideOnDesktop && $responsiveSliderItems > 4) {
                    var showToSlides = ($widthScreen >= "768" && $widthScreen <= "959") ? 2 : 4;
                    if (!$responsiveSliderDesktop.hasClass('slick-initialized')) {
                        $responsiveSliderDesktop.slick({
                            slidesToShow: showToSlides,
                            slidesToScroll: 1,
                            infinite: autoSlide,
                            focusOnSelect: true,
                            lazyLoad: 'ondemand',
                            centerMode: false,
                            dots: true,
                            autoplay: autoSlide,
                            autoplaySpeed: slideDelay,
                            mobileFirst: false
                        }); //slick it.
                    }
                } //desktop swipe
                if ($responsiveSlider.hasClass('slick-initialized')) {
                    $responsiveSlider.slick('unslick');
                }
            }
        } //slick responsive function

    var widthScreen = $(window).width();
    $(window).ready(showSliderScreen(widthScreen)).resize(
        function() {
            var widthScreen = $(window).width();
            showSliderScreen(widthScreen);
        }
    );



    //light box for gallery images
    $('body').on('click', '.lightbox-images>div>div>div', function() {
        //lighbox for tablet and desktop only.
        if ($(window).width() >= 768) {
            var imageSrc = $(this).find('article').css('background-image').slice('5').split('"')[0];
            var caption = $(this).find('article header').html();
            $('.light-box.image-box').fadeIn('fast', function() {
                $('.light-box.image-box>section img').attr('src', imageSrc);
                $('.light-box.image-box>section figcaption').html(caption);
                $('.light-box.image-box>section').slideDown('fast', function() {
                    var maxHeight = ($('.light-box>section img').height() <= 750) ? $('.light-box.image-box>section img').height() : 750;
                    $('.light-box.image-box>section').css('max-height', maxHeight + 'px');
                });
            });

            $('body').on('click', '.image-box a.close-bt', function(e) {
                e.preventDefault();
                $('.light-box.image-box>section').slideUp('fast', function() {
                    $('.light-box.image-box').fadeOut('fast');
                })
            });
        }
    });
};