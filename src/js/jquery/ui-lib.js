// UI-LIB-SCRIPTS
var $ = require('./vendors/jquery'),
  powerange = require('./vendors/powerange'),
  rangeSlider = require('./components/range-slider'),
  Prism = require('./vendors/prism'),
  copyToClipBoard = require('./utility/copy-to-clipboard'),
  emailThis = require('./utility/email-this'),
  searchFilter = require('./utility/search-filter'),
  slicks = require('./components/carousels.js'),
  datepicker = require('./utility/date-picker');

// Define the PEPPER_UI library namespace
var PEPPER_UI = {
  map: [], // Map of components
  docs: {}, // Docs of components
  uiMapReady: false,
  initUIMap: {}, // Init the component list
  initIFrameModel: {}, // Init the iFrame Model
  initMain: {}, // Init the outside UI Library,
  getDoc: {} // Get the doc of a component by its name.
};

PEPPER_UI.initUIMap = function() {

  $.getJSON('ui-library-map.json', function(data) {
    var queue = [];

    // Prepare PEPPER_UI.map
    PEPPER_UI.map = data;

    // By storing a series of asynchronous calls of JSON files in the 'queue' array
    $.each(data, function(key, parent) {
      $.each(parent, function(filename, fileinfo) {
        var docPath = fileinfo.docPath;

        if (docPath !== null && docPath.length > 0) {
          var fullDocPath = 'components/' + docPath;

          queue.push(
            $.ajax({
              dataType: "json",
              url: fullDocPath,
              group: key,
              success: function (docData) {
                // Prepare PEPPER_UI.docs
                PEPPER_UI.docs[filename] = docData;
              },
              error: function(data, status, error) {
                alert('Error. See console for more details.');
                console.group('UI Library: ' + status);
                console.log("File path: ", fullDocPath);
                console.log(error);
                console.groupEnd();
              }
            })
          );
        }
      });
    });

    // Trigger a function to build the UI Library Navigation when all items in the queue are queued.
    $.when.apply($, queue).then(
      _buildUILibraryNav,
      function() {
        // error occurred

    });
  });

  var _buildUILibraryNav = function() {
    var nav = $("#ui-library-nav");
    var nav_items = [];

    $.each(PEPPER_UI.map, function (key, parent) {
      var children = [], childrenDom, parentDom;

      $.each(parent, function (filename, child) {
        var notice,
          componentName = filename,
          componentDoc = PEPPER_UI.getDoc(filename);

        if (componentDoc === null || typeof componentDoc === 'undefined' || componentDoc.length === 0) {
          notice = '<i class="fa fa-exclamation-circle" title="Missing JSON componentDoc file"></i>'; // Missing JSON componentDoc file
        } else {
          notice = '';
          if (componentDoc.information !== null && typeof componentDoc.information !== 'undefined' && componentDoc.information.name.length > 0) {
            componentName = componentDoc.information.name;
          }
        }

        children.push('<li class="time-entry"><a href="components/' + child.htmlPath + '" data-filename="' + filename  + '">' + componentName + '</a>' + notice + '</li>');
      });

      childrenDom = $("<ul/>", {html: children.join("")});
      parentDom = $("<li/>", {html: '<div>' + key + '</div>'});
      childrenDom.appendTo(parentDom);
      nav_items.push(parentDom.html());
    });

    $("<ul/>", {
      "class": "ui-components",
      html: nav_items.join("")
    }).appendTo("#ui-library-nav").bind();
  };

};

PEPPER_UI.initIFrameModel = function() {
  $('body').on('click', '#bookmark-this', function (e) {
    var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    if ('addToHomescreen' in window && window.addToHomescreen.isCompatible) {
      // Mobile browsers
      addToHomescreen({autostart: false, startDelay: 0}).show(true);
    } else if (window.sidebar && window.sidebar.addPanel) {
      // Firefox version < 23
      window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
    } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
      // Firefox version >= 23 and Opera Hotlist
      $(this).attr({
        href: bookmarkURL,
        title: bookmarkTitle,
        rel: 'sidebar'
      }).off(e);
      return true;
    } else if (window.external && ('AddFavorite' in window.external)) {
      // IE Favorite
      window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {
      // Other browsers (mainly WebKit - Chrome/Safari)
      alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
    }

    return false;
  });

  var baseClass = ""; // base class of component
  var originalState = ""; // current classes applied on component

  //highlight the appropriate item in the left nav after click
  $('body').on('click', '.ui-components a', function (e) {

    e.preventDefault();
    if(!$(this).hasClass('active')) {
      $('.ui-components a').removeClass('active');
      $(this).addClass('active');



    var title = ($(this).attr('href').split("/")[2]).split(".")[0];
    var curHref = $(this).attr('href');
    var filename = $(this).attr('data-filename');
    var domainName = "https://ui.pepper.com.au/ui-library/index.html";
    // if(location.href.indexOf("localhost") > -1){
    //   domainName = "http://localhost:3010/index.html";
    // }
    
    $.get(curHref, function (data) {
      $('#component-display').html($(data));
      var clonecore = $('#component-display').clone();
      clonecore.find('details').remove();
      clonecore = clonecore.text(clonecore.html());
      //reset before appending actual data
      $('#html-code code').empty().html(clonecore);
      $('#component-display').parent().css("display", "block");


      //carousel command
      slicks();
      //for range slider
      datepicker();
      //initialize date picker
      
      if (title == 'shortform-slider' || title == 'pepper-form') {
        rangeSlider();
      }

      //html and css code styling
      Prism.plugins.NormalizeWhitespace.setDefaults({
        'remove-trailing': true,
        'remove-indent': true,
        'left-trim': true,
        'right-trim': true,
        'break-lines': 100,
        'indent': 0,
        'remove-initial-line-feed': true,
        'tabs-to-spaces': 1,
        'spaces-to-tabs': 0
      }); //prism normalise
      Prism.highlightAll();
      $('body').scrollTop(0);
      //reset every div before the call
      $('#StylesList').empty();
      $('#moduleName, .component-name').empty();
      $('#moduleDesc').empty();
      $('#moduleNotes').empty();
      $('#conLink').hide();
      $('#liveLink').hide();
      $('#baseClass').empty();
      $('#scriptClasses').hide();
      $('#eventTrigger').empty();
      $('#interactionClasses').empty();
      $('#shareComponent').empty();
      $('#shareComponent').attr("href","#");
      //fecth the json info file
      data = PEPPER_UI.getDoc(filename);

        var info = data.information;
        var css = data.cssClasses;
        var script = data.scriptClasses;

        baseClass = Object.keys(css.baseClass).toString(); // used for adding helperclasses
        originalState = $('.'+baseClass).attr("class");// use this to reset back 

        $('#moduleName, .component-name').html(info.name);
        $('#moduleDesc').html(info.description);
        $('#moduleNotes').html(info.notes);
        //if link is available then show else hide
        if (info.confluenceLink == "NA") {
          $('#conLink').hide();
        } else {
          $('#conLink').show();
          $('#conLink').attr('href', info.confluenceLink);
        }
        if (info.liveLink == "NA") {
          $('#liveLink').hide();
        } else {
          $('#liveLink').show();
          $('#liveLink').attr('href', info.liveLink);
        }
        $('#baseClass').html(baseClass);

        $('#shareComponent').html(domainName+"?c="+filename);
        $('#shareComponent').attr("href",domainName+"?c="+filename);

        //helper css classes
        var styles = css.helperClasses;        
        if (!$.isEmptyObject(styles)) {         
          $('#StylesList').closest('dl').show();
          for (var style in styles) {
            var styleDesc = styles[style];
            $('#StylesList').append('<label><input type="checkbox" data-class="' + style + '"><code class="checkbox">.' + style + '</code><span>' + styleDesc + '</span></label>');
          }
        } else {          
          $('#StylesList').closest('dl').hide();
        }

        runHelperClassState();

        //script classes
        var jsEvents = script.eventTrigger;
        var jsClasses = script.interactionClasses;
        if(!$.isEmptyObject(jsEvents) || !$.isEmptyObject(jsClasses)){

          $('#scriptClasses').show();

          if(!$.isEmptyObject(jsEvents)){
            $('#eventTrigger').closest('dd').show();
            for(var jsEvent in jsEvents) {
              var jsEventDesc = jsEvents[jsEvent];
              $('#eventTrigger').append('<label><code>'+jsEvent+'</code><span>'+jsEventDesc+'</span></label>')
            }
          }else{
            $('#eventTrigger').closest('dd').hide();
          }
          
          if(!$.isEmptyObject(jsClasses)){
            $('#interactionClasses').closest('dd').show();
            for(var jsClass in jsClasses) {
              var jsClassDesc = jsClasses[jsClass];
              $('#interactionClasses').append('<label><code>'+jsClass+'</code><span>'+jsClassDesc+'</span></label>')
            }
          }else{
            $('#interactionClasses').closest('dd').hide();
          }

        }else{
          $('#scriptClasses').hide();
        }
        
        });//get call

        //swith to component info tab
        $('.compo-nav a').trigger('click');
      }
  });//body on click

  //applied helper class will stay checked
  function runHelperClassState(){

    var chkboxArr = $('#StylesList input[type=checkbox]'); 
    for(var i = 0; i<chkboxArr.length; i++){
      if( $('#component-display').find('.'+baseClass).hasClass($(chkboxArr[i]).attr('data-class')) ){
        $(chkboxArr[i]).prop('checked',true);
      }
    }
  }

  //copy command
  $(".copy").on('click', function (event) {
    $(".copy").empty();
    (new copyToClipBoard()).copyToClipBoard('.language-markup');
    $(".copy").html('copy');
  });

  //copy component share link
  $("#compLinkCopy").on('click', function (event) {
    (new copyToClipBoard()).copyToClipBoard('.shareComponent');
  });
  //email component share link
  $("#compLinkEmail").on('click', function (event) {
    (new emailThis()).emailThis('Pepper UI Library: \"'+$('#moduleName').html()+'\" Link',$('#shareComponent').html());
  });

  //Toggle tabs of main side nav of ui
  $('.compo-nav a').on('click', function (e) {
    
    if (!$(this).hasClass('active')) {
      e.preventDefault();
      $('.compo-nav a').toggleClass('active');
      $('.ui-components, .lib-header fieldset, .compo-info-block').toggle('fast', function(){
        ResetScrollNav();
      });
      //reset all checked style in component info block;
      //reset the height of scroll area in side nav due to dynamic header
      if ($(this).is(':first-child')) {
        $('#StylesList input[type=checkbox]').prop('checked', false);
        $('.'+baseClass).attr("class",originalState);
        runHelperClassState();
      }
    }
  });

  //Hamburger Menu Close/Open
  $('body').on('click', '#StylesList input[type=checkbox]', function (e) {
    $('#component-display').find('.' + baseClass).toggleClass($(this).attr('data-class'));
  });

  //lightbox controls
  $(".call-html-modal").on('click', function (event) {
    $(".html-markup-lightbox").css("display", "block");
  });
  $(".html-markup-lightbox .close-bt").on('click', function (event) {
    $(".html-markup-lightbox").css("display", "none");
  });
  //expand - compress
  $(".html-markup-lightbox .screen-bt").on('click', function (event) {
    if ($(this).hasClass('expand')) {
      $(".html-markup-lightbox section").css("max-width", "100%");
      $(".html-markup-lightbox section").css("height", "100%");
      var windowHeight = $(window).height() - 100;
      $(".html-markup-lightbox section article").css("max-height", windowHeight + "px");
      $(this).removeClass('expand').addClass('compress').html('<i class="fa fa-compress"></i>');
    } else {
      $(".html-markup-lightbox section").css("max-width", "60em");
      $(".html-markup-lightbox section").css("height", "auto");
      $(".html-markup-lightbox section article").css("max-height", "240px");
      $(this).removeClass('compress').addClass('expand').html('<i class="fa fa-expand"></i>');
    }

  });
};

PEPPER_UI.initMain = function() {
  //search filter command
  $("#txtSearchPage").keyup(function () {
    (new searchFilter()).searchFilter($(".time-entry"), $(this).val());
  });

  //call the intro on page load
  setTimeout(function () {
    var defaultHref = "components/branding/introduction.html";
    var componentName = getParentQueryString('c');
    //console.log(componentName);

    if(componentName != ""){
      var componentHref = getComponentHref(componentName);
      if(typeof componentHref != "undefined"){
        defaultHref = componentHref;
      }
    }

    $("body .ui-components a[href='"+defaultHref+"']").trigger('click');
  }, 500);
};

PEPPER_UI.getDoc = function(filename) {
  return this.docs[filename];
};

$(document).ready(function(){
  PEPPER_UI.initMain();
  if (!PEPPER_UI.uiMapReady) {
    PEPPER_UI.initUIMap();
    PEPPER_UI.uiMapReady = true; // prevent the initUIMap() running again
  };

  PEPPER_UI.initIFrameModel();
});

function ResetScrollNav() {
  //console.log($('.compo-nav').outerHeight(true) + '<componav OH : lib-header OH >' + $('.lib-header').outerHeight(true));
  //console.log($('.compo-nav').height() + '<componav H : lib-header H >' + $('.lib-header').height());
  $('.compo-info-block').height($('#ui-library-nav').outerHeight(true) - ($('.compo-nav').outerHeight(true) + $('.lib-header').outerHeight(true) + 5));
  $('.ui-components').height($('#ui-library-nav').outerHeight(true) - ($('.compo-nav').outerHeight(true) + $('.lib-header').outerHeight(true)));
}

//access parent url to fetch query string
function getParentQueryString(qstr){
  var queryStr = parent.document.location.href.split("?"+qstr+"=")[1]
  return queryStr?queryStr.replace(/%20+_-/g, " "):"";
}

//access component url by sending in component name
function getComponentHref(componentName){
  return $(".ui-components ul li").find("a[data-filename='"+componentName+"']").attr("href");
}
