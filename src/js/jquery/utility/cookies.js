var $ = require('../vendors/jquery'),
 Cookies = require('js-cookie');
 
module.exports = function() {
  $(document).ready(function(){
    if($('.lending-disclaimer').length && Cookies.get('pepper-uk-disclaimer-notice') != "accepted") {
      $('.lending-disclaimer').show();
    } else {
        CookieNotice();
    }

    //disclaimer actions
    $('body').on('click', '.lending-disclaimer .disclaimer-accept, .lending-disclaimer .close-bt', function(e){
      e.preventDefault();
      $('.lending-disclaimer').hide('fast');
      Cookies.set('pepper-uk-disclaimer-notice', 'accepted', { expires: 365, path: '/' });
      CookieNotice();
    });
    
    $("body").on('click', '.slidedown-modal label.action', function (e) {
      Cookies.set('pepper-uk-cookie-notice', 'accepted', { expires: 365, path: '/' });
    });
    $('body').on('click', '.slidedown-modal .close-bt',function(){
      $('.slidedown-modal label.action').trigger('click');
    });
  });

  function CookieNotice() {
    if (Cookies.get("pepper-uk-cookie-notice") == "accepted") {
              $(".slidedown-modal").addClass('shown');
      }
      else {
        $(".slidedown-modal").removeClass('shown');
      }
  }

}