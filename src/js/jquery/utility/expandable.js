var $ = require('../vendors/jquery'),
checkDevices = require('../utility/check-device');

module.exports = function() {

function accordionToggle(el) {
    var component = $(el).closest('.expandable');
    var content = component.find('content');

    content.slideToggle('fast', function() {
        component.toggleClass('expanded');
    });
}

    // The expandable component
    $('body').on('click', '.expandable header', function(e) {
        e.preventDefault();
        accordionToggle($(this));
    });

    //check help-center disclaimer call
    if (window.location.href.indexOf('help-centre') > 0) {
        var isdcCall = checkDevices.getParameterByName('dc');
        if (isdcCall) {
            accordionToggle($('.expandable header'));
        }
    } //if condition
}