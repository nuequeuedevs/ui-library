var $ = require('../vendors/jquery');
module.exports = function() {

    $.fn.heightFix = function(cols, val) {
        if (val === 'reset') {
            this.height('auto');
            return this;
        }
        var maxHeight = 0;
        this.each(function() {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });
        this.height(maxHeight);
        return this;
    }; //global function

    function FixHeight() {
        // Fix height only applied to: .text-bullets and .text-image components
        var $bulletList = $('.tick-bullet.text-bullets ul li, .circle-bullet.text-bullets ul li, .tick-bullet.text-image ul li, .circle-bullet.text-image ul li');
        if ($(window).width() > 960) {
            $("input[name='grossIncomeFreq1']").next('span').css('font-size', '.72rem');
            $("input[name='grossIncomeFreq2']").next('span').css('font-size', '.72rem');
            $($bulletList).each(function() {
                if ($(this).index() % 2 == 0) {
                    if ($(this).outerHeight() > $(this).next().outerHeight()) {
                        $(this).next().height($(this).outerHeight());
                        $(this).height($(this).outerHeight());
                    } else {
                        // if($(this).index() == 2) { $(this).outerHeight() }
                        $(this).height($(this).next().outerHeight());
                        $(this).next().height($(this).next().outerHeight());
                    }
                }
            });   
        } else {
            $("input[name='grossIncomeFreq1']").next('span').css('font-size', '.6rem');
            $("input[name='grossIncomeFreq2']").next('span').css('font-size', '.6rem');
            $($bulletList).each(function() {
                $(this).css('height', 'auto');
            });
        }
        $('.three-box summary').heightFix();
        $('.circle-slider h3').heightFix();

        if ($(window).width() > 640) {
            $('.three-box h3').heightFix();
            $('.three-box div').heightFix();
            $('.tabs-swipe div').height('auto');
            $('.tabs-swipe div a').height('auto');
            $.fn.EqualRowHeight('.porthole-feature.desktop-swipe', '.porthole article', $('.porthole-feature.desktop-swipe .porthole').length);
            $.fn.EqualRowHeight('.porthole-feature.desktop-swipe', '.porthole figure', $('.porthole-feature.desktop-swipe .porthole').length);
            $.fn.EqualRowHeight('.porthole-feature.three-columns:not(.desktop-swipe)', '.porthole figure', 3);
            $.fn.EqualRowHeight('.porthole-feature.three-columns:not(.desktop-swipe)', '.porthole article', 3);
            $.fn.EqualRowHeight('.porthole-feature:not(.desktop-swipe):not(.three-columns)', '.porthole figure', 4);
            $.fn.EqualRowHeight('.porthole-feature:not(.desktop-swipe):not(.three-columns)', '.porthole article', 4);
            $.fn.EqualRowHeight('.page-tabs.active', '.featured', 2);
            $.fn.EqualRowHeight('.text-col2-hd-img', 'article', 2);
            $.fn.EqualRowHeight('.downloads', 'article .content', 2);
        } else {
            $('.tabs-swipe div').heightFix();
            $('.tabs-swipe div a').heightFix();
            $('.downloads .articles article').height('auto');
            $('.downloads .articles article header').height('auto');
            $('.downloads .articles .content').height('auto');
            $('.porthole-feature .porthole figure').height('auto');
            $('.porthole-feature .porthole article').height('auto');
            $('.porthole-feature.three-columns:not(.desktop-swipe)').height('auto');
            $('.porthole-feature.three-columns:not(.desktop-swipe)').height('auto');
            $('.page-tabs .featured').height('auto');
        }
        $('.video-gallery article ul li a').heightFix();
    }

    function SetTextFigure() {
        $('.text-figure article').each(function() {
            if ($(this).find('figure').length <= 0 && $(window).width() >= 960) {
                $(this).css('padding-bottom', '40px');
                divIn = $(this).find('div');
                divIn.css('width', '95%').css('max-width', '95%').css('text-align', 'center').css('margin-left', '0px');
            }
        });
    }

    $.fn.EqualRowHeight = function(parent, child, numofcolumns){
        $(parent).each(function(index) {
            if($(parent).hasClass('three-columns')){
                numofcolumns = 3;
            }
            if (($(window).width() > 640 && $(window).width() <= 945)  && $(parent).has('.porthole fieldset')) {
                numofcolumns = 2;
            }
            var numOfEleInRow = numofcolumns;
            var eleArr = $(this).find(child);
            var totalNumOfEle = eleArr.length;
            var numOfRows = Math.round(totalNumOfEle/numOfEleInRow);
            var rowDivs = new Array();
            var curPos = 0;
            var i = 0;
            while(true){
                var end = curPos + numOfEleInRow;
                end = end > eleArr.length ? eleArr.length : end;
                if(curPos > totalNumOfEle){
                    console.log(curPos);
                    break;
                }
                rowDivs[0+i] = eleArr.slice(curPos,end);
                i++;
                curPos = curPos + numofcolumns;
                
            }
           // console.log(rowDivs);
           var maxHeight = 0;
           for(var r = 0; r<rowDivs.length; r++){

                maxHeight = 0;
                for(var c=0; c<rowDivs[r].length ; c++){

                    if($(rowDivs[r][c]).outerHeight() > maxHeight){
                        maxHeight = $(rowDivs[r][c]).outerHeight();
                      
                    }

                } //columns

                for(var c=0; c<rowDivs[r].length ; c++) {
                    $(rowDivs[r][c]).height(maxHeight);
                }

           } //row

        });
    }

    function swapButtons() {
        if ($(window).width() <= 640) {
            $(".button-group").each(function() {
                $(this).find(".secondary").appendTo($(this));
            });
        } else {
            $(".button-group").each(function() {
                $(this).find(".secondary").prependTo($(this));
            });
        }
    }

    $(document).ready(function() {;
        setTimeout(function() {
            FixHeight();
        }, 700);
        SetTextFigure();
        swapButtons();
    });

    $(window).resize(function() {
        FixHeight();
        SetTextFigure();
        swapButtons();
    });
};