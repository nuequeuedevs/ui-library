var $ = require('../vendors/jquery');
module.exports = function() {

	this.copyToClipBoard = function(className) {
		// Select the email link anchor text  
      var nodeLink = document.querySelector(className);  
      var range = document.createRange(); 
      //console.log(range);
      range.selectNode(nodeLink);  
      window.getSelection().addRange(range);
      var status="";
        
      try {
        // Now that we've selected the anchor text, execute the copy command  
        var successful = document.execCommand('copy');  
        var msg = successful ? 'successful' : 'unsuccessful';  
        console.log('Copy command was ' + msg);  
        status = "copied!";
      } catch(err) {  
        console.log('Oops, unable to copy');  
        status = " unable to copy!";
      }  
      $('.message').html(status);
      $('.message').fadeIn(1000).delay(1000).fadeOut(1000);
      // Remove the selections - NOTE: Should use   
      // removeRange(range) when it is supported  
      window.getSelection().removeAllRanges();  
	}

}