var $ = require('../vendors/jquery');
module.exports = function() {
    //for adding commas, extension methods
    String.prototype.addComma = function() {
            return this.replace(/(.)(?=(.{3})+$)/g, "$1,")
        }
        //Jquery global extension method
    $.fn.manageCommas = function() {
        // case 8:  // Backspace
        //         //console.log('backspace');
        //     case 9:  // Tab
        //     case 13: // Enter
        //     case 37: // Left
        //     case 38: // Up
        //     case 39: // Right
        //     case 40: // Down
        return this.each(function() {
            $(this).val($(this).val().replace(/(,| )/g, '').addComma());
        })
    }

    $.fn.santizeCommas = function() {
        return $(this).val($(this).val().replace(/(,| )/g, ''));
    }

    $.fn.addCommasToString = function() {
        return $(this).replace(/(,| )/g, '').addComma();
    }

}