var should = require('should'),
    formValidation = require('../form-validation.js');

describe("Form validation module: isValidDate()", function() {
  var texts = [
    { val: '12/08/1989', expectedResult: true },
    { val: '1a/0a/198s', expectedResult: false },
    { val: '11-25-9999', expectedResult: false },
    { val: '1a/90/1000', expectedResult: false }
  ];

  texts.forEach(function(text) {
    it("should return " + text.expectedResult + " for '" + text.val + "'", function() {
      formValidation.isValidDate(text.val).should.be.equal(text.expectedResult);
    });
  });
});