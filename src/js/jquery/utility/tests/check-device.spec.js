var checkDevice = require('../check-device.js'),
    should = require('should');

describe("Check device module", function() {
  it("should be a defined object", function() {
    checkDevice.should.be.an.instanceOf(Object);
  });

  it("should have a function called 'isIOS'", function(){
    checkDevice.should.have.property('isIOS');
  });
});