var $ = require('../vendors/jquery');
module.exports = function() {
//number incrementer js
    $("body").on('click', '.number-incrementer a', function(e) {
        e.preventDefault();
        var minVal = parseInt($(this).parent().find('input').data('min'));
        var maxVal = parseInt($(this).parent().find('input').data('max'));
        var incrementVal = parseInt($(this).parent().find('input').data('increment'));
        var currentVal = parseInt($(this).parent().find('input').val());
        var inputEl = $(this).parent().find('input');
        switch ($(this).text()) {
            case '-':
                if (currentVal > minVal) {
                    inputEl.val(currentVal - incrementVal);
                    $(this).parent().find('a').removeClass('disabled');
                    if (inputEl.val() == 0) {
                        $(this).addClass('disabled');
                    }
                }
                break;
            case '+':
                if (currentVal < maxVal) {
                    inputEl.val(currentVal + incrementVal);
                    $(this).parent().find('a').removeClass('disabled');
                    if (inputEl.val() == maxVal) {
                        $(this).addClass('disabled');
                    }
                }
                break;
        }
    }); //number incrementer

    //prevent input other than numeric [0-9]
    $('body .number-incrementer input').keyup(function(key) {
        if (key.charCode < 48 || key.charCode > 57 || $(this).val().length == 2) return false;
    });

    //validate number incrementer input
    $('body').on('blur', '.number-incrementer input', function(e) {
        e.preventDefault();
        var minVal = parseInt($(this).data('min'));
        var maxVal = parseInt($(this).data('max'));
        if ($(this).val() >= maxVal) {
            if ($(this).val() > maxVal) {
                $(this).val(maxVal);
                alert('Maximum value can only be ' + maxVal);
            }
            //Question: Shall we alert user for max value????
            $(this).parent().find('a:last').addClass('disabled');
            $(this).parent().find('a:first').removeClass('disabled');

        } else if ($(this).val() <= minVal || $(this).val() == '' || $(this).val() == null) {
            $(this).val(minVal);
            if ($(this).val() == minVal) {
                $(this).parent().find('a:first').addClass('disabled');
            }
            $(this).parent().find('a:last').removeClass('disabled');
        } else {
            $(this).parent().find('a').removeClass('disabled');
        }
    });

}