var $ = require('../vendors/jquery');

exports.isIOS = function () {
  return (
    (navigator.userAgent.toLowerCase().indexOf("ipad") > -1) ||
    (navigator.userAgent.toLowerCase().indexOf("iphone") > -1) ||
    (navigator.userAgent.toLowerCase().indexOf("ipod") > -1)
  );
};

exports.getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

exports.isiPhone = function () {
  return (
    (navigator.userAgent.toLowerCase().indexOf("iphone") > -1) ||
    (navigator.userAgent.toLowerCase().indexOf("ipod") > -1)
  );
};

//ie 9 check browser sniffing bad :)
exports.isIE9 = function () {
  return (
    (navigator.userAgent.toLowerCase().indexOf("trident/5.0") > -1) ||
    (navigator.userAgent.toLowerCase().indexOf("msie 9.0") > -1)
  );
};

exports.isAndriod = function () {
  var ua = navigator.userAgent;
  var is_native_android = ((ua.indexOf('Mozilla/5.0') > -1 && ua.indexOf('Android ') > -1 && ua.indexOf('AppleWebKit') > -1) && (ua.indexOf('Version') > -1));
  return is_native_android;
};


exports.isMacChrome = function () {
  var OSName;
  if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
  var isChrome = !!window.chrome;
  if (OSName == "MacOS" && isChrome == true) {
    return true;
  } else {
    return false;
  }
};

exports.isDesktopScreen = function() {
  var viewportWidth = $(window).width();
  return (viewportWidth > 960);
};

module.exports = exports;