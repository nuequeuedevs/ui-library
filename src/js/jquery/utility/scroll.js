var $ = require('../vendors/jquery');

exports.scrollToSection = function(sectionId, callback) {
  var section = ($(sectionId).length > 0) ? $(sectionId) : $('.scroll-here'),
      appNav = $("nav[class='app-nav']"),
      appNavHeight = appNav.height(),
      desktopNav = $("nav[class='desktop-nav']"),
      desktopNavHeight = desktopNav.is(':hidden') ? 0 : desktopNav.height(),
      sectionMarginTop = parseInt(section.css('margin-top')),
      sectionTop;

  sectionTop = section.offset().top - appNavHeight - desktopNavHeight - sectionMarginTop;

  $('html,body').animate({
    scrollTop: sectionTop
  }, 500, callback);
};

module.exports = exports;

/*
  $('.scroll-to-page a').on('click', function(e) {
    var howFar = 360;
    e.preventDefault();
    if ($(this).closest('.hero-video').hasClass('scroll-to-page')) {
      scrollPage($('.scroll-here'), howFar);
    } else {
      scrollPageTo($('.scroll-here'), howFar);
    }
  });

  /!*
   * Scroll page and set focus on the first form found
   *!/
  function scrollPageTo(el, lessMargin) {
    scrollPage(el, lessMargin, function() {
      $('fieldset input:first').focus();
    });
  }


  /!*
   * Scroll page to a position
   *!/
  function scrollPage(el, lessMargin, callback) {
    var position = el.offset().top + 60;
    if (lessMargin !== undefined && lessMargin !== 0) {
      if ($(window).width() <= 768) {
        lessMargin = 120;
      }
      position = position - parseInt(lessMargin);
    }
    $('html,body').animate({
      scrollTop: position
    }, 500, callback);
  }
}*/