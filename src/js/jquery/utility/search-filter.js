var $ = require('../vendors/jquery');
module.exports = function() {

  $.expr[":"].containsNoCase = function (el, i, m) {
      var search = m[3];
      if (!search) return false;
        return new RegExp(search,"i").test($(el).text());
  };

  this.searchFilter = function(el, search) {
    el.show();
    if (search)
        el.not(":containsNoCase(" + search + ")").hide();
  }

}