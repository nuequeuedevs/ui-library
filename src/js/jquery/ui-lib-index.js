var $ = require('./vendors/jquery')

//swicth site command
$('body').on('change', '#SwitchSite', function (e) {
  e.preventDefault();
  if ($(this).val() != undefined) {
    $('#frame').attr('src', $(this).val());
    $('.break-points').show()
  }
});

$(function() {
  //breakpoint control command
  $(".break-points li").click(function(event) {

    event.preventDefault();
    var bpvalue = $(this).attr("id");
    var iframeLoc = $('#frame').attr('src');
    var iframeBlock = "";

    if(!$(this).hasClass('active')) {
      $(".break-points li").removeClass('active');
      $(this).addClass('active');
      if(iframeLoc == "iframe-model.html"){
        iframeBlock = $('#frame').contents();
      }

      if(bpvalue == 'auto'){
        if(iframeLoc == "iframe-model.html"){
          iframeBlock.find('#Hamburger').prop('checked', false);
        }
        $("#frame").css('position','fixed');
      } else {
        if(iframeLoc == "iframe-model.html"){
          iframeBlock.find('#Hamburger').prop('checked', true);
        }
        $("#frame").css('position','relative');
      }
      $(".breakpoint-animation").animate({width: bpvalue, height: "900px"}, 700);
    } //if not active
  });//breakpoint js

  //home btn
  $(".home").on('click', function (event) {
    //call the intro on page.
    event.preventDefault();
    location.reload();
  });

});// doc ready