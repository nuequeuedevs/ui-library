'use strict';

var config        = require('../gulp.config');
var handleErrors  = require('../util/handleErrors');
var logger        = require('../util/logger');
var gulp          = require('gulp');
var gutil = require('gulp-util');
var fileInclude   = require('gulp-file-include');
var browserSync  = require('browser-sync');

gulp.task('html', ['default'], function() {
  var app = global.app,
      html_src = config.apps[app].html.src,
      html_dest = (config.build.html).replace('{app}', app);

  return gulp.src(html_src)
    .pipe(fileInclude({
      prefix: '@@',
      basepath: '@file',
      context: {
        name: 'test'
      }
    }))
    .pipe(gulp.dest(html_dest))
    .on('error', gutil.log)
    .on('end', function() {
      logger("Updated HTML");
    })
    .pipe(browserSync.stream({ once: true }));
});

