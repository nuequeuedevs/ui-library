'use strict';

var config    = require('../gulp.config');
var gulp      = require('gulp');
var gutil     = require('gulp-util');
var args      = require('yargs').argv;
var inquirer  = require('inquirer');
var _         = require('underscore');

gulp.task('default', function(done) {
  if (_.isUndefined(global.app)) {
    var choices = _.keys(config.apps);

    inquirer.prompt([
      {
        type: 'rawlist',
        message: '\n Which application do you want to run?',
        name: 'app',
        default: 'ui-library',
        choices: choices
      }
    ], function (answers) {
      global.app = answers.app;

      if (_.isUndefined(config.apps[global.app])) {
        throw new gutil.PluginError('clean', {
          message: 'The "' + global.app + '" app is not defined in gulp.config'
        })
      } else {
        done();
      }
    });
  } else {
    done();
  }
});

