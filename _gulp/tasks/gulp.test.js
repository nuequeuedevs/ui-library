'use strict';

var config = require('../gulp.config');
var logger = require('../util/logger');
var gulp = require('gulp');
var mocha = require('gulp-mocha');
var watch = require('gulp-watch');


 

gulp.task('watch-test',['default'], function() {
  var app = global.app;
  //logger('==================='+ config.apps[app].js.specs);
  gulp.watch(config.apps[app].js.specs, ['test']);
});

gulp.task('test', ['default','watch-test'], function() {
  var app = global.app,
    tests = config.apps[app].js.specs;

    //logger('==================='+ tests);

  return gulp.src(tests, { read: false })
    .pipe(mocha({
      reporter: 'list'
    }));
});