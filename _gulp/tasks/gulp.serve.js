'use strict';

var config        = require('../gulp.config');
var logger        = require('../util/logger');
var gulp          = require('gulp');
var runSequence  = require('run-sequence');

gulp.task('serve', ['clean'], function(done) {
  var app = global.app;

  if (app == 'ui-library') {
    runSequence(['sass', 'js', 'ui-library-map', 'html'], 'watch', done);
  } else {
    runSequence(['sass', 'js', 'html'], 'watch', done);
  }
});

