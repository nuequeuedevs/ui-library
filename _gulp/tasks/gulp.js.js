'use strict';

var config        = require('../gulp.config');
var handleErrors  = require('../util/handleErrors');
var logger        = require('../util/logger');
var gulp          = require('gulp');
var gutil         = require('gulp-util');
var browserSync   = require('browser-sync');
var browserify    = require('gulp-browserify');
var uglify         = require('gulp-uglify');
var rename        = require('gulp-rename');

gulp.task('js', ['default'], function() {
  var app = global.app,
      main_js = config.apps[app].js.app,
      js_dest = (app.indexOf('Career') > -1) ? (config.buildtheme.js).replace('{app}', app) : (config.build.js).replace('{app}', app);

  return gulp.src(main_js)
    .pipe(browserify({
      debug: false
    }))
    .on('error', handleErrors)
    .pipe(gulp.dest(js_dest))

    // minified version
    .pipe(uglify({
      compress: { drop_console: true },
      'ascii-only': true
    }))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(js_dest))

    .on('end', function() {
      logger("Updated JS");
    })
    .pipe(browserSync.stream({ once: true }));
});

