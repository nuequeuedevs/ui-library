'use strict';

var config  = require('../gulp.config');
var logger  = require('../util/logger');
var gulp    = require('gulp');
var del     = require('del');

gulp.task('clean', ['default'], function(done) {
  var app = global.app,
      html_files = (config.build.html).replace('{app}', app) + "/**/*.html",
      css_files = (config.build.css).replace('{app}', app),
      js_files = (config.build.js).replace('{app}', app),
      files_to_delete = [ html_files, css_files, js_files ];

  if (app === 'ui-library') {
    var ui_library_map = (config.build.html).replace('{app}', app) + "/ui-library-map.json";
    files_to_delete.push(ui_library_map);
  }

  return del(files_to_delete, function() {
    logger('Deleted "' + html_files + '"');
    logger('Deleted "' + css_files + '"');
    logger('Deleted "' + js_files + '"');
    done();
  });

});

