'use strict';

var config = require('../gulp.config'),
    logger = require('../util/logger'),
    gulp = require('gulp'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    watch = require('gulp-watch');

gulp.task('watch', ['browserSync'], function() {
    var app = global.app;

    gulp.watch(config.apps[app].sass.src, ['sass'], reload);
    gulp.watch(config.apps[app].js.src, ['js'], reload);

    watch(config.apps[app].html.src, function() {
        gulp.start('html');
    });
    gulp.watch([config.apps[app].html.src], ['html', reload]);

    if (app == 'ui-library') {
        watch(config.apps[app].docs.src, function() {
            gulp.start('docs');
            gulp.start('ui-library-map');
        });

        watch(config.apps[app].html.map, function() {
            gulp.start('ui-library-map');
        });
    }
});