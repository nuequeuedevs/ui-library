'use strict';

var config        = require('../gulp.config');
var handleErrors  = require('../util/handleErrors');
var logger        = require('../util/logger');
var gulp          = require('gulp');
var gutil         = require('gulp-util');
var browserSync   = require('browser-sync');

gulp.task('docs', ['default'], function() {
  var app = global.app,
    docs = config.apps[app].docs.src,
    dest = (config.build.docs).replace('{app}', app);

  // Only generating document for UI Library
  if (app == 'ui-library') {
    return gulp.src(docs)
      .pipe(gulp.dest(dest))
      .on('error', gutil.log)
      .on('end', function () {
        logger("Copy JSON docs to build");
      })
      .pipe(browserSync.stream({once: true}));
  } else {
    done();
  }
});

