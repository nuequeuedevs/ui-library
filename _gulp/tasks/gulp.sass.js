'use strict';

var config       = require('../gulp.config');
var handleErrors = require('../util/handleErrors');
var gulp         = require('gulp');
var gutil = require('gulp-util');
var rename       = require('gulp-rename');
var minifyCss    = require('gulp-minify-css');
var sourcemaps   = require('gulp-sourcemaps');
var sass         = require('gulp-sass');
var browserSync  = require('browser-sync');
var logger        = require('../util/logger');

gulp.task('sass', ['default'], function () {
  var app = global.app,
      main_scss = config.apps[app].sass.app,
      sass_dest = (app.indexOf('Career') > -1) ? (config.buildtheme.css).replace('{app}', app) : (config.build.css).replace('{app}', app),
      sass_includes = config.apps[app].sass.includePaths;

  return gulp.src(main_scss)
    .pipe(sass({
      sourceComments: false,
      outputStyle: 'nested',
      includePaths: sass_includes,
      require: ['susy', 'breakpoint', 'compass-mixins']
    }))
    .on('error', handleErrors)
    .pipe(gulp.dest(sass_dest)) // non-minified version

    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(sass_dest)) // minified version

    .on('end', function() {
      logger('Updated CSS');
    })

    .pipe(browserSync.stream({ once: true }));
});
