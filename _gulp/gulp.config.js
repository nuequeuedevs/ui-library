'use strict';

module.exports = {

  'browserPort'  : 3010,
  'UIPort'       : 3011,
  'serverPort'   : 3012,
  'testPort'     : 3100,

  'build': {
    'html': 'build/{app}',
    'css': 'build/{app}/css',
    'js': 'build/{app}/js',
    'docs': 'build/{app}/components'
  },
  'buildtheme': {
    'html': 'build/{app}/theme',
    'css': 'build/{app}/theme/css',
    'js': 'build/{app}/theme/js',
    'docs': 'build/{app}/components'
  },

  'apps': {
    'ui-library': {
      'html': {
        'src': [
          './src/html/**/*.html',
          '!./src/html/apps/**/*.html'
        ],
        'map': ['./src/html/components/**/*.html']
      },
      'docs': {
        'src': ['./src/html/components/**/*.json']
      },
      'sass': {
        'src': ['./src/sass/**/*.scss'],
        'app': [
          './src/sass/apps/ui-library.main.scss',
          './src/sass/apps/pwc.main.scss',
          './src/sass/apps/serviceability-calculator.scss',
          './src/sass/apps/uk.main.scss',
          './src/sass/apps/sharepoint-brand.scss'
        ],
        'includePaths': [
          './node_modules/susy/sass',
          './node_modules/breakpoint-sass/stylesheets',
          './node_modules/compass-mixins/lib'
        ]
      },
      'js': {
        'src': ['./src/js/**/*.js'],
        'app': ['./src/js/jquery/pwc.script.js', './src/js/jquery/uk.script.js', './src/js/jquery/ui-lib-index.js', './src/js/jquery/ui-lib.js', './src/js/jquery/pepper-lpf.js'],
        'specs': ['./src/js/**/*.spec.js']
      }
    },
    'getmyscore': {
      'html': {
        'src': ['./src/html/apps/getmyscore/**/*.html']
      },
      'sass': {
        'src': ['./src/sass/**/*.scss'],
        'app': [
          './src/sass/apps/getmyscore/getmyscore.main.scss'
        ],
        'includePaths': [
          './node_modules/susy/sass',
          './node_modules/breakpoint-sass/stylesheets',
          './node_modules/compass-mixins/lib'
        ]
      },
      'js': {
        'src': ['./src/js/**/*.js'],
        'app': [
          './src/js/apps/getmyscore/getmyscore.script.js'
        ]
      }
    },
    'paf': {
      'html': {
        'src': ['./src/html/apps/paf/**/*.html']
      },
      'sass': {
        'src': ['./src/sass/**/*.scss'],
        'app': [
          './src/sass/apps/paf/paf.main.scss'
        ],
        'includePaths': [
          './node_modules/susy/sass',
          './node_modules/breakpoint-sass/stylesheets',
          './node_modules/compass-mixins/lib'
        ]
      },
      'js': {
        'src': ['./src/js/**/*.js'],
        'app': [
          './src/js/apps/paf/paf.script.js'
        ]
      }
    }, //paf
    'interflex': {
      'html': {
        'src': ['./src/html/apps/interflex/**/*.html']
      },
      'sass': {
        'src': ['./src/sass/**/*.scss'],
        'app': [
          './src/sass/apps/interflex/interflex.main.scss'
        ],
        'includePaths': [
          './node_modules/susy/sass',
          './node_modules/breakpoint-sass/stylesheets',
          './node_modules/compass-mixins/lib'
        ]
      },
      'js': {
        'src': ['./src/js/**/*.js'],
        'app': [
          './src/js/apps/interflex/interflex.script.js'
        ]
      }
    }, //interflex
    'CareerPortal': {
      'html': {
        'src': ['./src/html/apps/careerportal/**/*.html']
      },
      'sass': {
        'src': ['./src/sass/**/*.scss'],
        'app': [
          './src/sass/apps/careerportal/taleo.scss'
        ],
        'includePaths': [
          './node_modules/susy/sass',
          './node_modules/breakpoint-sass/stylesheets',
          './node_modules/compass-mixins/lib'
        ]
      },
      'js': {
        'src': ['./src/js/**/*.js'],
        'app': [
          './src/js/apps/careerportal/careerportal.script.js'
        ]
      }
    } //interflex
  }
};
