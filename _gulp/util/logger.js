'use strict';

/* logger
 * ------------
 * Log message into the console
 */

var gutil        = require('gulp-util');

module.exports = function(message) {
  gutil.log(gutil.colors.yellow(" [-]"), gutil.colors.yellow(message));
};