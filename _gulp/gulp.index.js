'use strict';

var fs = require('fs');
var onlyScripts = require('./util/scriptFilter');
var tasks = fs.readdirSync('./_gulp/tasks/').filter(onlyScripts);
var config = require('./gulp.config');

tasks.forEach(function(task) {
  require('./tasks/' + task);
});